<?php
/* +*******************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ******************************************************************************** */

require_once 'include/Webservices/CreateTarget.php'; #added by goku
require_once 'include/Webservices/Delete.php';
require_once 'include/Webservices/DescribeObject.php';

#function added by goku
function vtws_retrieve_Contact($id, $user)
{
	global $log,$adb;

	//$webserviceObject = VtigerWebserviceObject::fromId($adb,$id);
	$webserviceObject = VtigerWebserviceObject::fromName($adb, 'SocialContacts');
	$handlerPath = $webserviceObject->getHandlerPath();
	$handlerClass = $webserviceObject->getHandlerClass();
	
	require_once $handlerPath;
	
	$handler = new $handlerClass($webserviceObject,$user,$adb,$log);
	$meta = $handler->getMeta();
	$entityName = $meta->getObjectEntityName($id);
	$types = vtws_listtypes(null, $user);
	if(!in_array($entityName,$types['types']))
	{
		throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to perform the operation is denied");
	}

	if($meta->hasReadAccess()!==true)
	{
		throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to write is denied");
	}

	if($entityName !== $webserviceObject->getEntityName())
	{
		throw new WebServiceException(WebServiceErrorCode::$INVALIDID,"Id specified is incorrect");
	}
		
	if(!$meta->hasPermission(EntityMeta::$RETRIEVE,$id))
	{
		throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to read given object is denied");
	}
		
	$idComponents = vtws_getIdComponents($id);
	if(!$meta->exists($idComponents[1]))
	{
		throw new WebServiceException(WebServiceErrorCode::$RECORDNOTFOUND,"Record you are trying to access is not found");
	}

	$entity = $handler->retrieve($id);
	VTWS_PreserveGlobal::flush();
	return $entity;
}

function vtws_retrieve_Lead($id, $user)
{
	global $log,$adb;

	//$webserviceObject = VtigerWebserviceObject::fromId($adb,$id);
	$webserviceObject = VtigerWebserviceObject::fromName($adb, 'SocialContacts');
	$handlerPath = $webserviceObject->getHandlerPath();
	$handlerClass = $webserviceObject->getHandlerClass();
	
	require_once $handlerPath;
	
	$handler = new $handlerClass($webserviceObject,$user,$adb,$log);
	$meta = $handler->getMeta();

	$entityName = $meta->getObjectEntityName($id);
	$types = vtws_listtypes(null, $user);

	if(!in_array($entityName,$types['types']))
	{
		throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to perform the operation is denied");
	}

	if($meta->hasReadAccess()!==true)
	{
		throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to write is denied");
	}

	if($entityName !== $webserviceObject->getEntityName())
	{
		throw new WebServiceException(WebServiceErrorCode::$INVALIDID,"Id specified is incorrect");
	}
		
	if(!$meta->hasPermission(EntityMeta::$RETRIEVE,$id))
	{
		throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to read given object is denied");
	}
		
	$idComponents = vtws_getIdComponents($id);
	if(!$meta->exists($idComponents[1]))
	{
		throw new WebServiceException(WebServiceErrorCode::$RECORDNOTFOUND,"Record you are trying to access is not found");
	}

	$entity = $handler->retrieve($id);
	VTWS_PreserveGlobal::flush();
	return $entity;
}


function vtws_convertContact($entityvalues, $user) 
{
	global $adb, $log;
	if (empty($entityvalues['assignedTo'])) {
		$entityvalues['assignedTo'] = vtws_getWebserviceEntityId('Users', $user->id);
	}
	if (empty($entityvalues['transferRelatedRecordsTo'])) {
		$entityvalues['transferRelatedRecordsTo'] = 'Contacts';
	}

	$leadObject = VtigerWebserviceObject::fromName($adb, 'SocialContacts');

	$handlerPath = $leadObject->getHandlerPath();
	$handlerClass = $leadObject->getHandlerClass();

	require_once $handlerPath;

	$leadHandler = new $handlerClass($leadObject, $user, $adb, $log);
	$leadInfo = vtws_retrieve_Contact($entityvalues['leadId'], $user);

	$entityIds = array();
	//$availableModules = array('Accounts', 'Contacts', 'Potentials');
	$availableModules = array('Contacts');
	if (!(($entityvalues['entities']['Accounts']['create']) || ($entityvalues['entities']['Contacts']['create']))) {
		return null;
	}
	
	foreach ($availableModules as $entityName) 
	{
		if ($entityvalues['entities'][$entityName]['create']) 
		{
			$entityvalue = $entityvalues['entities'][$entityName];
			$entityObject = VtigerWebserviceObject::fromName($adb, $entityvalue['name']);
			$handlerPath = $entityObject->getHandlerPath();
			$handlerClass = $entityObject->getHandlerClass();

			require_once $handlerPath;

			$entityHandler = new $handlerClass($entityObject, $user, $adb, $log);

			$entityObjectValues = array();
			$entityObjectValues['assigned_user_id'] = $entityvalues['assignedTo'];
			$entityObjectValues = vtws_populateConvertLeadEntities($entityvalue, $entityObjectValues, $entityHandler, $leadHandler, $leadInfo);
			
			//update potential related to property
			if ($entityvalue['name'] == 'Potentials') 
			{
				if (!empty($entityIds['Accounts'])) 
				{
					$entityObjectValues['related_to'] = $entityIds['Accounts'];
				} 
				else 
				{
					$entityObjectValues['related_to'] = $entityIds['Contacts'];
				}
			}

			//update the contacts relation
			if ($entityvalue['name'] == 'Contacts') {
				if (!empty($entityIds['Accounts'])) {
					$entityObjectValues['account_id'] = $entityIds['Accounts'];
				}
			}

			try {
				$create = true;
				if ($entityvalue['name'] == 'Accounts') {
					$sql = "SELECT vtiger_account.accountid FROM vtiger_account,vtiger_crmentity WHERE vtiger_crmentity.crmid=vtiger_account.accountid AND vtiger_account.accountname=? AND vtiger_crmentity.deleted=0";
					$result = $adb->pquery($sql, array($entityvalue['accountname']));
					if ($adb->num_rows($result) > 0) {
						$entityIds[$entityName] = vtws_getWebserviceEntityId('Accounts', $adb->query_result($result, 0, 'accountid'));
						$create = false;
					}
				}
				if ($create) { 
					$entityRecord = vtws_createContact($entityvalue['name'], $entityObjectValues, $user);
					$entityIds[$entityName] = $entityRecord['id'];
				}
			} catch (Exception $e) {
				return null;
			}
		}
	}


	try {
		$accountIdComponents = vtws_getIdComponents($entityIds['Accounts']);
		$accountId = $accountIdComponents[1];

		$contactIdComponents = vtws_getIdComponents($entityIds['Contacts']);
		$contactId = $contactIdComponents[1];
		
		if (!empty($accountId) && !empty($contactId) && !empty($entityIds['Potentials'])) {
			$potentialIdComponents = vtws_getIdComponents($entityIds['Potentials']);
			$potentialId = $potentialIdComponents[1];
			$sql = "insert into vtiger_contpotentialrel values(?,?)";
			$result = $adb->pquery($sql, array($contactId, $potentialIdComponents[1]));
			if ($result === false) {
				throw new WebServiceException(WebServiceErrorCode::$FAILED_TO_CREATE_RELATION,
						"Failed to related Contact with the Potential");
			}
		}

		$transfered = vtws_convertLeadTransferHandler($leadIdComponents, $entityIds, $entityvalues);
		$relatedIdComponents = vtws_getIdComponents($entityIds[$entityvalues['transferRelatedRecordsTo']]);
		$check = vtws_getRelatedActivitiesTarget($leadIdComponents[1], $accountId, $contactId, $relatedIdComponents[1]);
		vtws_updateConvertLeadStatus($entityIds, $entityvalues['leadId'], $user);
		# Code added by goku to Assign Target to contacts
		$contactid = explode('x',$entityIds['Contacts']);
		$socialcontactsid  = explode('x',$entityvalues['leadId']);
		assignrelated_smack($contactid[1], $id[1],'Contacts');
		# Code Ends here
	
		# Code added by goku to remove the Target
		$id = explode('x', $entityvalues['leadId']);
		$adb->pquery("update vtiger_crmentity set deleted = 1 where crmid = ?",array($id[1]));
		# Code Ends here
	} catch (Exception $e) {
		foreach ($entityIds as $entity => $id) {
			vtws_deleteSocialContacts($id, $user);
		}
		return null;
	}

	return $entityIds;
}

function vtws_convertLead($entityvalues, $user) 
{
	global $adb, $log;
	if (empty($entityvalues['assignedTo'])) {
		$entityvalues['assignedTo'] = vtws_getWebserviceEntityId('Users', $user->id);
	}
	if (empty($entityvalues['transferRelatedRecordsTo'])) {
		$entityvalues['transferRelatedRecordsTo'] = 'Contacts';
	}

	$leadObject = VtigerWebserviceObject::fromName($adb, 'SocialContacts');
	$handlerPath = $leadObject->getHandlerPath();
	$handlerClass = $leadObject->getHandlerClass();

	require_once $handlerPath;

	$leadHandler = new $handlerClass($leadObject, $user, $adb, $log);
	$leadInfo = vtws_retrieve_Lead($entityvalues['leadId'], $user);

	$entityIds = array();
	$availableModules = array('Leads');
	if (!(($entityvalues['entities']['Accounts']['create']) || ($entityvalues['entities']['Leads']['create']))) {
		return null;
	}

	foreach ($availableModules as $entityName) 
	{
		if ($entityvalues['entities'][$entityName]['create']) 
		{
			$entityvalue = $entityvalues['entities'][$entityName];
			$entityObject = VtigerWebserviceObject::fromName($adb, $entityvalue['name']);
			$handlerPath = $entityObject->getHandlerPath();
			$handlerClass = $entityObject->getHandlerClass();

			require_once $handlerPath;

			$entityHandler = new $handlerClass($entityObject, $user, $adb, $log);

			$entityObjectValues = array();
			$entityObjectValues['assigned_user_id'] = $entityvalues['assignedTo'];

			$entityObjectValues = vtws_populateConvertLeadEntities($entityvalue, $entityObjectValues, $entityHandler, $leadHandler, $leadInfo);
			
			//update potential related to property
			if ($entityvalue['name'] == 'Potentials') 
			{
				if (!empty($entityIds['Accounts'])) 
				{
					$entityObjectValues['related_to'] = $entityIds['Accounts'];
				} 
				else 
				{
					$entityObjectValues['related_to'] = $entityIds['Contacts'];
				}
			}

			//update the contacts relation
			if ($entityvalue['name'] == 'Contacts') {
				if (!empty($entityIds['Accounts'])) {
					$entityObjectValues['account_id'] = $entityIds['Accounts'];
				}
			}

			try {
				$create = true;
				if ($entityvalue['name'] == 'Accounts') {
					$sql = "SELECT vtiger_account.accountid FROM vtiger_account,vtiger_crmentity WHERE vtiger_crmentity.crmid=vtiger_account.accountid AND vtiger_account.accountname=? AND vtiger_crmentity.deleted=0";
					$result = $adb->pquery($sql, array($entityvalue['accountname']));
					if ($adb->num_rows($result) > 0) {
						$entityIds[$entityName] = vtws_getWebserviceEntityId('Accounts', $adb->query_result($result, 0, 'accountid'));
						$create = false;
					}
				}

				if ($create) {
					$entityRecord = vtws_createLead($entityvalue['name'], $entityObjectValues, $user);
					$entityIds[$entityName] = $entityRecord['id'];
				}

			} catch (Exception $e) {
				return null;
			}
		}
	}

	try {
		$accountIdComponents = vtws_getIdComponents($entityIds['Accounts']);
		$accountId = $accountIdComponents[1];

		$contactIdComponents = vtws_getIdComponents($entityIds['Leads']);
		$contactId = $contactIdComponents[1];

		if (!empty($accountId) && !empty($contactId) && !empty($entityIds['Potentials'])) {
			$potentialIdComponents = vtws_getIdComponents($entityIds['Potentials']);
			$potentialId = $potentialIdComponents[1];
			$sql = "insert into vtiger_contpotentialrel values(?,?)";
			$result = $adb->pquery($sql, array($contactId, $potentialIdComponents[1]));
			if ($result === false) {
				throw new WebServiceException(WebServiceErrorCode::$FAILED_TO_CREATE_RELATION,
						"Failed to related Contact with the Potential");
			}
		}

		$transfered = vtws_convertLeadTransferHandler($leadIdComponents, $entityIds, $entityvalues);
		$relatedIdComponents = vtws_getIdComponents($entityIds[$entityvalues['transferRelatedRecordsTo']]);
		//$check = vtws_getRelatedActivitiesTarget($leadIdComponents[1], $accountId, $contactId, $relatedIdComponents[1]);
		vtws_updateConvertLeadStatus1($entityIds, $entityvalues['leadId'], $user);
		# Code added by goku to Assign Target to contacts

		$contactid = explode('x',$entityIds['Leads']);
		$targetid  = explode('x',$entityvalues['leadId']);
		assignrelated_smack($contactid[1], $targetid[1],'Leads');
		# Code Ends here
	
		# Code added by goku to remove the Target
		$id = explode('x', $entityvalues['leadId']);
		$adb->pquery("update vtiger_crmentity set deleted = 1 where crmid = ?",array($id[1]));
		# Code Ends here
	} catch (Exception $e) {
		foreach ($entityIds as $entity => $id) {
			vtws_deleteTarget($id, $user);
		}
		return null;
	}
	return $entityIds;
}

/*
 * 	This function will assign the related modules to the current module (Leads/Contacts)
 *	Params $toid -> Lead/Contact id, $fromid -> targetid, module name
 */

function assignrelated_smack($toid,$fromid, $module)
{	
	global $adb;
	#Assigning Products to vtiger_seproductsrel table
	$getProducts = $adb->pquery("select crmid, relcrmid from vtiger_crmentityrel where crmid = ? and relmodule = ?",array($fromid, 'Products'));
	while($fetch_products = $adb->fetch_array($getProducts))
	{
		$products[] = $fetch_products;
	}
	
	foreach($products as $sin_pro)
	{
		$cc = $adb->pquery("insert into vtiger_seproductsrel values (?, ?, ?)",array($toid, $sin_pro['relcrmid'], $module));
	}

	#Assigning Campaign to vtiger_campaigncontrel table
	if($module == 'Contacts')
	{
		$products       = array();
		$fetch_products = array();
		$getProducts = $adb->pquery("select crmid, relcrmid from vtiger_crmentityrel where crmid = ? and relmodule = ?",array($fromid, 'Campaigns'));
		while($fetch_products = $adb->fetch_array($getProducts))
		{
			$products[] = $fetch_products;
		}
		foreach($products as $sin_pro)
		{
			$raj = $adb->pquery("insert into vtiger_campaigncontrel values (?, ?, ?)",array($sin_pro['relcrmid'], $toid, 1));
		}
	}
	else if($module == 'Leads')
	{
		$products       = array();
		$fetch_products = array();
		$getProducts = $adb->pquery("select crmid, relcrmid from vtiger_crmentityrel where crmid = ? and relmodule = ?",array($fromid, 'Campaigns'));
		while($fetch_products = $adb->fetch_array($getProducts))
		{
			$products[] = $fetch_products;
		}print_r($products);
		foreach($products as $sin_pro)
		{
			$raj = $adb->pquery("insert into vtiger_campaignleadrel values (?, ?, ?)",array($sin_pro['relcrmid'], $toid, 1));
		}
	}
	$adb->pquery("update vtiger_crmentityrel set crmid = ? where crmid = ?",array($toid, $fromid));
}


	function vtws_deleteTarget($id,$user){
		global $log,$adb;
		$webserviceObject = VtigerWebserviceObject::fromId($adb,$id);
		$handlerPath = $webserviceObject->getHandlerPath();
		$handlerClass = $webserviceObject->getHandlerClass();
		require_once $handlerPath;
		
		$handler = new $handlerClass($webserviceObject,$user,$adb,$log);
		$meta = $handler->getMeta();
		$entityName = $meta->getObjectEntityName($id);
		
		$types = vtws_listtypes(null, $user);
		if(!in_array($entityName,$types['types'])){
			throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to perform the operation is denied");
		}
		
		if($entityName !== $webserviceObject->getEntityName()){
			throw new WebServiceException(WebServiceErrorCode::$INVALIDID,"Id specified is incorrect");
		}
		
		if(!$meta->hasPermission(EntityMeta::$DELETE,$id)){
			throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to read given object is denied");
		}
		
		$idComponents = vtws_getIdComponents($id);
		if(!$meta->exists($idComponents[1])){
			throw new WebServiceException(WebServiceErrorCode::$RECORDNOTFOUND,"Record you are trying to access is not found");
		}
		
		if($meta->hasDeleteAccess()!==true){
			throw new WebServiceException(WebServiceErrorCode::$ACCESSDENIED,"Permission to delete is denied");
		}
		$entity = $handler->delete($id);
		VTWS_PreserveGlobal::flush();
		return $entity;
	}

function vtws_getRelatedActivitiesTarget($leadId,$accountId,$contactId,$relatedId) 
{
	if(empty($relatedId) && empty($contactId))
	{
		throw new WebServiceException(WebServiceErrorCode::$LEAD_RELATED_UPDATE_FAILED,
			"Failed to move related Activities/Emails");
	}
	global $adb;
	$sql = "select * from vtiger_seactivityrel where crmid=?";
	$result = $adb->pquery($sql, array($leadId));
	if($result === false)
	{
		return false;
	}
	$rowCount = $adb->num_rows($result);
	for($i=0;$i<$rowCount;++$i) 
	{
		$activityId=$adb->query_result($result,$i,"activityid");

		$sql ="select setype from vtiger_crmentity where crmid=?";
		$resultNew = $adb->pquery($sql, array($activityId));
		if($resultNew === false){
			return false;
		}
		$type=$adb->query_result($resultNew,0,"setype");

		$sql="delete from vtiger_seactivityrel where crmid=?";
		$resultNew = $adb->pquery($sql, array($leadId));
		if($resultNew === false)
		{
			return false;
		}

		if($type != "Emails") 
		{
			if(!empty($accountId))
			{
				$sql = "insert into vtiger_seactivityrel(crmid,activityid) values (?,?)";
				$resultNew = $adb->pquery($sql, array($accountId, $activityId));
				if($resultNew === false)
				{
					return false;
				}
			}
			if(!empty($contactId))
			{
				$sql="insert into vtiger_cntactivityrel(contactid,activityid) values (?,?)";
				$resultNew = $adb->pquery($sql, array($contactId, $activityId));
				if($resultNew === false)
				{
					return false;
				}
			}
		} 
		else 
		{
			$sql = "insert into vtiger_seactivityrel(crmid,activityid) values (?,?)";
			$resultNew = $adb->pquery($sql, array($relatedId, $activityId));
			if($resultNew === false)
			{
				return false;
			}
		}
	}
	return true;
}

/**
 * Function used to transfer all the Target related records to given Entity(Contact/Account) record
 * @param $leadid - targetid
 * @param $relatedid - related entity id (contactid/accountid)
 * @param $setype - related module(Accounts/Contacts)
 */
function vtws_transferLeadRelatedRecordsTarget($leadId, $relatedId, $seType) {

	if(empty($leadId) || empty($relatedId) || empty($seType)){
		throw new WebServiceException(WebServiceErrorCode::$LEAD_RELATED_UPDATE_FAILED,
			"Failed to move related Records");
	}
	$status = vtws_getRelatedNotesAttachments($leadId, $relatedId);

	if($status === false){
		throw new WebServiceException(WebServiceErrorCode::$LEAD_RELATED_UPDATE_FAILED,
			"Failed to move related Documents to the ".$seType);
	}
	//Retrieve the lead related products and relate them with this new account
	$status = vtws_saveLeadRelatedProducts($leadId, $relatedId, $seType);
	if($status === false){
		throw new WebServiceException(WebServiceErrorCode::$LEAD_RELATED_UPDATE_FAILED,
			"Failed to move related Products to the ".$seType);
	}
	$status = vtws_saveLeadRelations($leadId, $relatedId, $seType);
	if($status === false){
		throw new WebServiceException(WebServiceErrorCode::$LEAD_RELATED_UPDATE_FAILED,
			"Failed to move Records to the ".$seType);
	}
	$status = vtws_saveLeadRelatedCampaigns($leadId, $relatedId, $seType);
	if($status === false){
		throw new WebServiceException(WebServiceErrorCode::$LEAD_RELATED_UPDATE_FAILED,
			"Failed to move Records to the ".$seType);
	}
	vtws_transferComments($leadId, $relatedId);
}


/*
 * populate the entity fields with the lead info.
 * if mandatory field is not provided populate with '????'
 * returns the entity array.
 */

function vtws_populateConvertLeadEntities($entityvalue, $entity, $entityHandler, $leadHandler, $leadinfo) {
	global $adb, $log;
	$column;
	$entityName = $entityvalue['name'];
	$sql = "SELECT * FROM vtiger_convertleadmapping";
	$result = $adb->pquery($sql, array());
	if ($adb->num_rows($result)) {
		switch ($entityName) {
			case 'Accounts':$column = 'accountfid';
				break;
			case 'Contacts':$column = 'contactfid';
				break;
			case 'Potentials':$column = 'potentialfid';
				break;
			default:$column = 'leadfid';
				break;
		}

		$leadFields = $leadHandler->getMeta()->getModuleFields();
		$entityFields = $entityHandler->getMeta()->getModuleFields();
		$row = $adb->fetch_array($result);
		$count = 1;
		do {
			$entityField = vtws_getFieldfromFieldId($row[$column], $entityFields);
			if ($entityField == null) {
				//user doesn't have access so continue.TODO update even if user doesn't have access
				continue;
			}
			$leadField = vtws_getFieldfromFieldId($row['leadfid'], $leadFields);
			if ($leadField == null) {
				//user doesn't have access so continue.TODO update even if user doesn't have access
				continue;
			}
			$leadFieldName = $leadField->getFieldName();
			$entityFieldName = $entityField->getFieldName();
			$entity[$entityFieldName] = $leadinfo[$leadFieldName];
			$count++;
		} while ($row = $adb->fetch_array($result));

		foreach ($entityvalue as $fieldname => $fieldvalue) {
			if (!empty($fieldvalue)) {
				$entity[$fieldname] = $fieldvalue;
			}
		}

		$entity = vtws_validateConvertLeadEntityMandatoryValues($entity, $entityHandler, $leadinfo, $entityName);
	}
	return $entity;
}

function vtws_validateConvertLeadEntityMandatoryValues($entity, $entityHandler, $leadinfo, $module) 
{
	$mandatoryFields = $entityHandler->getMeta()->getMandatoryFields();
	foreach ($mandatoryFields as $field) 
	{
		if (empty($entity[$field])) 
		{
			$fieldInfo = vtws_getConvertLeadFieldInfo($module, $field);
			if (($fieldInfo['type']['name'] == 'picklist' || $fieldInfo['type']['name'] == 'multipicklist'
				|| $fieldInfo['type']['name'] == 'date' || $fieldInfo['type']['name'] == 'datetime')
				&& ($fieldInfo['editable'] == true)) {
				$entity[$field] = $fieldInfo['default'];
			} else {
				$entity[$field] = '????';
			}
		}
	}
	return $entity;
}

function vtws_getConvertLeadFieldInfo($module, $fieldname) 
{
	global $adb, $log, $current_user;
	$describe = vtws_describe($module, $current_user);
	foreach ($describe['fields'] as $index => $fieldInfo) 
	{
		if ($fieldInfo['name'] == $fieldname) 
		{
			return $fieldInfo;
		}
	}
	return false;
}

//function to handle the transferring of related records for lead
function vtws_convertLeadTransferHandler($leadIdComponents, $entityIds, $entityvalues) 
{
	try 
	{
		$entityidComponents = vtws_getIdComponents($entityIds[$entityvalues['transferRelatedRecordsTo']]);
		vtws_transferLeadRelatedRecordsTarget($leadIdComponents[1], $entityidComponents[1], $entityvalues['transferRelatedRecordsTo']);
	}
	catch (Exception $e) 
	{
		return false;
	}

	return true;
}

function vtws_updateConvertLeadStatus($entityIds, $leadId, $user) 
{
	global $adb, $log;
	$leadIdComponents = vtws_getIdComponents($leadId);
	if ($entityIds['Accounts'] != '' || $entityIds['Contacts'] != '') 
	{
		$sql = "DELETE FROM vtiger_tracker WHERE item_id=?";
		$adb->pquery($sql, array($leadIdComponents[1]));

		//update the modifiedtime and modified by information for the record
		$leadModifiedTime = $adb->formatDate(date('Y-m-d H:i:s'), true);
		$crmentityUpdateSql = "UPDATE vtiger_crmentity SET modifiedtime=?, modifiedby=? WHERE crmid=?";
		$adb->pquery($crmentityUpdateSql, array($leadModifiedTime, $user->id, $leadIdComponents[1]));
	}
}

function vtws_updateConvertLeadStatus1($entityIds, $leadId, $user) 
{
	global $adb, $log;
	$leadIdComponents = vtws_getIdComponents($leadId);
	if ($entityIds['Accounts'] != '' || $entityIds['Leads'] != '') 
	{
		$sql = "DELETE FROM vtiger_tracker WHERE item_id=?";
		$adb->pquery($sql, array($leadIdComponents[1]));

		//update the modifiedtime and modified by information for the record
		$leadModifiedTime = $adb->formatDate(date('Y-m-d H:i:s'), true);
		$crmentityUpdateSql = "UPDATE vtiger_crmentity SET modifiedtime=?, modifiedby=? WHERE crmid=?";
		$adb->pquery($crmentityUpdateSql, array($leadModifiedTime, $user->id, $leadIdComponents[1]));
	}
}
?>
