<?php
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is:  vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
require_once('config.inc.php');
global $adb;

$smarty = new vtigerCRM_Smarty;

if($_REQUEST['socioaction'] == 'Save')
{
	$query = $adb->pquery("select * from vtiger_sociosettings",array());
	$count = $adb->num_rows($query);
	if($count != 0)
	{
		$adb->pquery("update vtiger_sociosettings set twitter = ?, twitter_usecret = ?, twitter_utoken = ?, twitter_csecret = ?, twitter_ckey = ?,  facebook_app = ?, facebook_secret = ?, linkedintoken1 = ?, linkedintoken2 = ?, linkedinauth1 = ?, linkedinauth2= ? where id = 1",array($_REQUEST['twitter'], $_REQUEST['twitter_usecret'], $_REQUEST['twitter_utoken'], $_REQUEST['twitter_csecret'], $_REQUEST['twitter_ckey'], $_REQUEST['facebookappid'], $_REQUEST['facebooksecret'], $_REQUEST['linkedintoken1'], $_REQUEST['linkedintoken2'], $_REQUEST['linkedinauth1'], $_REQUEST['linkedinauth2']));
	}
	else
	{
		$adb->pquery("insert into vtiger_sociosettings (id, twitter, twitter_usecret, twitter_utoken, twitter_csecret, twitter_ckey, facebook_app, facebook_secret, linkedintoken1, linkedintoken2, linkedinauth1, linkedinauth2) values (1,?,?,?,?,?,?,?,?,?,?,?)",array($_REQUEST['twitter'], $_REQUEST['twitter_usecret'], $_REQUEST['twitter_utoken'], $_REQUEST['twitter_csecret'], $_REQUEST['twitter_ckey'], $_REQUEST['facebookappid'], $_REQUEST['facebooksecret'], $_REQUEST['linkedintoken1'], $_REQUEST['linkedintoken2'], $_REQUEST['linkedinauth1'], $_REQUEST['linkedinauth2']));
	}

	$smarty->assign("TWITTER" , $_REQUEST['twitter']);
	$smarty->assign("TWITTER_CKEY" , $_REQUEST['twitter_ckey']);
	$smarty->assign("TWITTER_CSECRET" , $_REQUEST['twitter_csecret']);
	$smarty->assign("TWITTER_UTOKEN" , $_REQUEST['twitter_utoken']);
	$smarty->assign("TWITTER_USECRET" , $_REQUEST['twitter_usecret']);

	$smarty->assign("FACEBOOK_APP", $_REQUEST['facebookappid']);
	$smarty->assign("FACEBOOK_SECRET", $_REQUEST['facebooksecret']);
	$smarty->assign("LINKEDIN_TOKEN1", $_REQUEST['linkedintoken1']);
	$smarty->assign("LINKEDIN_TOKEN2", $_REQUEST['linkedintoken2']);
	$smarty->assign("LINKEDIN_AUTH1", $_REQUEST['linkedinauth1']);
	$smarty->assign("LINKEDIN_AUTH2", $_REQUEST['linkedinauth2']);
}
else
{
        $query = $adb->pquery("select * from vtiger_sociosettings",array());
        $count = $adb->num_rows($query);

	if($_REQUEST['socioaction'] == 'Edit')
	{
		$smarty->assign("MODE", "EDIT");
	}

        if($count != 0)
        {
		while($fetch_settings = $adb->fetch_array($query))
		{
			$getSocial = $fetch_settings;
		}

	        $smarty->assign("TWITTER_CKEY" , $getSocial['twitter_ckey']);
        	$smarty->assign("TWITTER_CSECRET" , $getSocial['twitter_csecret']);
	        $smarty->assign("TWITTER_UTOKEN" , $getSocial['twitter_utoken']);
        	$smarty->assign("TWITTER_USECRET" , $getSocial['twitter_usecret']);

	        $smarty->assign("TWITTER" , $getSocial['twitter']);
	        $smarty->assign("FACEBOOK_APP", $getSocial['facebook_app']);
        	$smarty->assign("FACEBOOK_SECRET", $getSocial['facebook_secret']);
        	$smarty->assign("LINKEDIN_TOKEN1", $getSocial['linkedintoken1']);
	        $smarty->assign("LINKEDIN_TOKEN2", $getSocial['linkedintoken2']);
        	$smarty->assign("LINKEDIN_AUTH1", $getSocial['linkedinauth1']);
	        $smarty->assign("LINKEDIN_AUTH2", $getSocial['linkedinauth2']);
	}
}
$smarty->display('Settings/SocioSettings.tpl');

?>
