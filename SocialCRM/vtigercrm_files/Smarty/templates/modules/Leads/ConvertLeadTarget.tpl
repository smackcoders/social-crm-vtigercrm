{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is:  vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

{assign var=row value=$UIINFO->getLeadInfo()}

<form name="ConvertLead" method="POST" action="index.php" onsubmit="VtigerJS_DialogBox.block();">
	<input type="hidden" name="module" value="Leads">
	<input type="hidden" name="transferToName" value="{$row.company}">
	<input type="hidden" name="record" value="{$UIINFO->getLeadId()}">
	<input type="hidden" name="action">
	<input type="hidden" name="parenttab" value="{$CATEGORY}">
	<input type="hidden" name="current_user_id" value="{$UIINFO->getUserId()}'">

	<div id="orgLay" style="display: block;" class="layerPopup" >

		<table width="100%" border="0" cellpadding="5" cellspacing="0" class="layerHeadingULine">
			<tr>
				<td class="genHeaderSmall" align="left"><img src="{'Leads.gif'|@vtiger_imageurl:$THEME}">{'ConvertLead'|@getTranslatedString:$MODULE} : {$row.firstname} {$row.lastname}</td>
				<td align="right"><a href="javascript:fninvsh('orgLay');"><img src="{'close.gif'|@vtiger_imageurl:$THEME}" align="absmiddle" border="0"></a></td>
			</tr>
		</table>
		<table border=0 cellspacing=0 cellpadding=5 width=95% align=center>

			{if $UIINFO->isModuleActive('Leads')}
			<tr>
				<td class="small">
					<table border="0" cellspacing="0" cellpadding="0" width="95%" align="center" bgcolor="white">
						<tr>
							<td colspan="4" class="detailedViewHeader">
<!--								<input type="checkbox" checked="checked" onclick="javascript:showHideStatus('contact_block',null,null);" id="select_contact" name="entities[]" value="Contacts"></input> -->
								<b> Leads </b>
							</td>
						</tr>
						<tr>
							<td>
								<div id="contact_block" style="display:block;" >
									<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center" bgcolor="white">
										<tr>
											<td align="right" width="50%" class="dvtCellLabel">{if $UIINFO->isMandatory('Contacts','lastname') eq true}<font color="red">*</font>{/if}{'Last Name'|@getTranslatedString:$MODULE}</td>
											<td class="dvtCellInfo"><input type="text" name="lastname" {if $UIINFO->isMandatory('Contacts','lastname') eq true}record="true"{/if} module="Contacts" class="detailedViewTextBox" value="{$UIINFO_SMACK.socialcontactname}"></td>
										</tr>
										{if $UIINFO->isActive('firstname','Contacts')}
										<tr>
											<td align="right" width="50%" class="dvtCellLabel">{if $UIINFO->isMandatory('Contacts','firstname') eq true}<font color="red">*</font>{/if}{'First Name'|@getTranslatedString:$MODULE}</td>
											<td class="dvtCellInfo"><input type="text" name="firstname" class="detailedViewTextBox" module="Contacts" value="{$UIINFO->getMappedFieldValue('Contacts','firstname',0)}" {if $UIINFO->isMandatory('Contacts','firstname') eq true}record="true"{/if} ></td>
										</tr>
										{/if}
										{if $UIINFO->isActive('email','Contacts')}
										<tr>
											<td align="right" width="50%" class="dvtCellLabel">{if $UIINFO->isMandatory('Contacts','email') eq true}<font color="red">*</font>{/if}{'SINGLE_Emails'|@getTranslatedString:$MODULE}</td>
											<td class="dvtCellInfo"><input type="text" name="email" class="detailedViewTextBox" value="{$UIINFO_SMACK.socialcontactemail}" {if $UIINFO->isMandatory('Contacts','email') eq true}record="true"{/if} module="Contacts"></td>
										</tr>
										{/if}
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
			</tr>
			{/if}
			<tr>
				<td class="small">
					<table border="0" cellspacing="0" cellpadding="5" width="95%" align="center" bgcolor="white">
						<tr>
							<td align="right" class="dvtCellLabel" width="50%" style="border-top:1px solid #DEDEDE;">{'LBL_LIST_ASSIGNED_USER'|@getTranslatedString:$MODULE}</td>
							<td class="dvtCellInfo" width="50%" style="border-top:1px solid #DEDEDE;">
								<input type="radio" name="c_assigntype" value="U" onclick="javascript: c_toggleAssignType(this.value)" {$UIINFO->getUserSelected()} />&nbsp;{'LBL_USER'|@getTranslatedString:$MODULE}
								{if $UIINFO->getOwnerList('group')|@count neq 0}
								<input type="radio" name="c_assigntype" value="T" onclick="javascript: c_toggleAssignType(this.value)" {$UIINFO->getGroupSelected()} />&nbsp;{'LBL_GROUP'|@getTranslatedString:$MODULE}
								{/if}
								<span id="c_assign_user" style="display:{$UIINFO->getUserDisplay()}">
									<select name="c_assigned_user_id" class="detailedViewTextBox">
										{foreach item=user from=$UIINFO->getOwnerList('user') name=userloop}
											<option value="{$user.userid}" {if $user.selected eq true}selected="selected"{/if}>{$user.username}</option>
										{/foreach}
									</select>
								</span>
								<span id="c_assign_team" style="display:{$UIINFO->getGroupDisplay()}">
									{if $UIINFO->getOwnerList('group')|@count neq 0}
									<select name="c_assigned_group_id" class="detailedViewTextBox">
										{foreach item=group from=$UIINFO->getOwnerList('group') name=grouploop}
											<option value="{$group.groupid}" {if $group.selected eq true}selected="selected"{/if}>{$group.groupname}</option>
										{/foreach}
									</select>
									{/if}
								</span>
							</td>
						</tr>
						<tr>
							<td align="right" class="dvtCellLabel" width="50%">{'LBL_TRANSFER_RELATED_RECORDS_TO'|@getTranslatedString:$MODULE}</td>
							<td class="dvtCellInfo" width="50%">
								{if $UIINFO->isModuleActive('Leads') eq true}<input type="radio" name="transferto" id="transfertocon" value="Leads" checked="checked" onclick="selectTransferTo('Leads')"  /> Leads {/if}
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</table>
			<table border=0 cellspacing=0 cellpadding=5 width=100% class="layerPopupTransport">
			<tr>
					<td align="center">
						<input name="Save" value="{'LBL_SAVE_BUTTON_LABEL'|@getTranslatedString:$MODULE}" onclick="javascript:this.form.action.value='LeadConvertToEntitiesTarget'; return verifyConvertLeadData(ConvertLead)" type="submit"  class="crmbutton save small">&nbsp;&nbsp;
						<input type="button" name=" Cancel " value="{'LBL_CANCEL_BUTTON_LABEL'|@getTranslatedString:$MODULE}" onClick="hide('orgLay')" class="crmbutton cancel small">
					</td>
				</tr>
			</table>
	</div>
</form>


