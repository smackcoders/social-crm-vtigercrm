<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is:  vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->
{literal}
<script type = "text/javascript">
function setAccessKey()
{
	window.location.href = "index.php?module=SocialContacts&action=FB";
}
function setAccessKeyLinkedIn(){
        window.location.href = "index.php?module=SocialContacts&action=getAccessTokenLN";
}
</script>
{/literal}
<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
	<div align=center>
	<form action="#" method="post" name="sociosettings">
		{include file="SetMenu.tpl"}
		<table class="settingsSelUITopLine" width="100%" cellspacing="0" cellpadding="5" border="0">
		<tbody>
			<tr>
				<td width="50" valign="top" rowspan="2">
					<img width="48" height="48" border="0" title="Customize Record Numbering" alt="Customize Record Numbering" src="themes/images/settingsInvNumber.gif">
				</td>
				<td class="heading2" valign="bottom">
					<b> <a href="index.php?module=Settings&action=index&parenttab=Settings">Settings</a>
					> Social Details
					</b>
				</td>
			</tr>
			<tr>
				<td class="small" valign="top"> Save / Edit Facebook, Twitter, LinkedIn id </td>
				<td> <div id = "showop"> </div> </td>
			</tr>
		</tbody>
		</table>	

		<table border=0 cellspacing=0 cellpadding=5 width=100% class="tableHeading">
			<tr>
				<td class="big"> <strong> Social Details </strong> </td>
				<td class="small" align=right>
				{if $MODE eq 'EDIT'}
					<input type = "submit" value = "Save" name = "socioaction" class="crmButton small save">
				{else}
					<input type = "submit" value = "Edit" name = "socioaction" class="crmButton small save">
				{/if}
				</td>
			</tr>
		</table>
					
		<table border=0 cellspacing=0 cellpadding=5 width=100% class="listRow">
			<tr>
				<td class="small cellText big" width="30%" ><strong> Twitter User Name </strong></td>
				{if $MODE eq 'EDIT'}
				       <td class = "small cellText" width = "70%"> <input type = "text" name = "twitter" id = "twitter" value = "{$TWITTER}" class="detailedViewTextBox small"> </td>
				{else} 
					<td class="small cellText" width="70%"> {$TWITTER} </td> 
				{/if}
			</tr>
			<tr>
				<td class="small cellText big" width="30%" ><strong> Twitter Consumer Key </strong></td>
				{if $MODE eq 'EDIT'}
				       <td class = "small cellText" width = "70%"> <input type = "text" name = "twitter_ckey" id = "twitter_key" value = "{$TWITTER_CKEY}" class="detailedViewTextBox small"> </td>
				{else} 
					<td class="small cellText" width="70%"> {$TWITTER_CKEY} </td> 
				{/if}
			</tr>
			<tr>
				<td class="small cellText big" width="30%" ><strong> Twitter Consumer Secret </strong></td>
				{if $MODE eq 'EDIT'}
				       <td class = "small cellText" width = "70%"> <input type = "text" name = "twitter_csecret" id = "twitter_csecret" value = "{$TWITTER_CSECRET}" class="detailedViewTextBox small"> </td>
				{else} 
					<td class="small cellText" width="70%"> {$TWITTER_CSECRET} </td> 
				{/if}
			</tr>
			<tr>
				<td class="small cellText big" width="30%" ><strong> Twitter User Token </strong></td>
				{if $MODE eq 'EDIT'}
				       <td class = "small cellText" width = "70%"> <input type = "text" name = "twitter_utoken" id = "twitter_token" value = "{$TWITTER_UTOKEN}" class="detailedViewTextBox small"> </td>
				{else} 
					<td class="small cellText" width="70%"> {$TWITTER_UTOKEN} </td> 
				{/if}
			</tr>
			<tr>
				<td class="small cellText big" width="30%" ><strong> Twitter User Secret </strong></td>
				{if $MODE eq 'EDIT'}
				       <td class = "small cellText" width = "70%"> <input type = "text" name = "twitter_usecret" id = "twitter_usecret" value = "{$TWITTER_USECRET}" class="detailedViewTextBox small"> </td>
				{else} 
					<td class="small cellText" width="70%"> {$TWITTER_USECRET} </td> 
				{/if}
			</tr>
			<tr valign="top" bgcolor = "#C0C0C0">
			        <td class="small cellText big" width="30%"><strong> FaceBook App ID </strong></td>
                                {if $MODE eq 'EDIT'} 
                                       <td class = "small cellText" width = "70%"> <input type = "text" name = "facebookappid" id = "facebookappid" value = "{$FACEBOOK_APP}" class="detailedViewTextBox small" > </td>
                                {else} 
                                        <td class="small cellText" width="70%"> {$FACEBOOK_APP} </td>     
                                {/if}
			</tr>
                        <tr valign="top" bgcolor = "#C0C0C0">
                                <td class="small cellText big" width="30%"><strong> FaceBook Secret Key </strong></td>
                                {if $MODE eq 'EDIT'}
                                       <td class = "small cellText" width = "70%"> <input type = "text" name = "facebooksecret" id = "facebooksecret" value = "{$FACEBOOK_SECRET}" class="detailedViewTextBox small" > </td>
                                {else}
                                        <td class="small cellText" width="70%"> {$FACEBOOK_SECRET} <input type = "button" value = "Get Access Key" style = "float:right;" onclick = "setAccessKey();" > </td>
                                {/if}
                        </tr>
			<tr bgcolor = "#8FBCDB">
				<td class="small cellText big" width="30%"><strong> LinkedIn API Key </strong></td>
                                {if $MODE eq 'EDIT'} 
                                       <td class = "small cellText" width = "70%"> <input type = "text" name = "linkedintoken1" id = "linkedintoken1" value = "{$LINKEDIN_TOKEN1}" class="detailedViewTextBox small"> </td>
                                {else} 
                                        <td class="small cellText" width="70%"> {$LINKEDIN_TOKEN1} </td>     
                                {/if}
			</tr>
                        <tr bgcolor = "#8FBCDB">
                                <td class="small cellText big" width="30%"><strong> LinkedIn Secret Key </strong></td>
                                {if $MODE eq 'EDIT'}
                                       <td class = "small cellText" width = "70%"> <input type = "text" name = "linkedintoken2" id = "linkedintoken2" value = "{$LINKEDIN_TOKEN2}" class="detailedViewTextBox small"> </td>
                                {else}
                                        <td class="small cellText" width="70%"> {$LINKEDIN_TOKEN2} <input type = "button" value = "Get Access Key" style = "float:right;" onclick = "setAccessKeyLinkedIn();" ></td>
                                {/if}
                        </tr>
                        <tr bgcolor = "#8FBCDB">
                                <td class="small cellText big" width="30%"><strong> LinkedIn User Token </strong></td>
                                {if $MODE eq 'EDIT'}
                                       <td class = "small cellText" width = "70%"> <input type = "text" name = "linkedinauth1" id = "linkedinauth1" value = "{$LINKEDIN_AUTH1}" class="detailedViewTextBox small"> </td>
                                {else}
                                        <td class="small cellText" width="70%"> {$LINKEDIN_AUTH1} </td>
                                {/if}
                        </tr>
                        <tr bgcolor = "#8FBCDB">
                                <td class="small cellText big" width="30%"><strong> LinkedIn User Secret </strong></td>
                                {if $MODE eq 'EDIT'}
                                       <td class = "small cellText" width = "70%"> <input type = "text" name = "linkedinauth2" id = "linkedinauth2" value = "{$LINKEDIN_AUTH2}" class="detailedViewTextBox small"> </td>
                                {else}
                                        <td class="small cellText" width="70%"> {$LINKEDIN_AUTH2} </td>
                                {/if}
                        </tr>

		</table>

	</form>	
	</div>
</table>
