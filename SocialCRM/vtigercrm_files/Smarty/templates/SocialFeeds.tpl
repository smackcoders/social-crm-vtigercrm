{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is:  vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
<!--  USER  SETTINGS PAGE STARTS HERE -->
<div id="status" style="position: absolute; display: none; left: 850px; top: 95px; height: 27px; white-space: nowrap;">
<img src="themes/softed/images/status.gif">
</div>
<script language="JavaScript" type="text/javascript" src="modules/SocialFeeds/SocialFeeds.js"></script>
<script language="JavaScript" type="text/javascript" src="modules/SocialFeeds/Ajaxfileupload-jquery-1.3.2.js"></script>
<script language="JavaScript" type="text/javascript" src="modules/SocialFeeds/ajaxupload.3.5.js"></script>
<script language="JavaScript" type="text/javascript" src="modules/SocialFeeds/imgupload.js"></script>
<script language="JavaScript" type="text/javascript" src="modules/SocialFeeds/imgupload1.js"></script>

<link rel="stylesheet" type="text/css" href="modules/SocialFeeds/SocialFeeds.css">
<!-- Shadow starts here -->
<!--
<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
	<tr>

		<td class="showPanelBg" valign="top" width="95%" align="center" >
			<br>
			<table width="100%"  border="0" cellspacing="0" cellpadding="0" class="mailClient">
				<tr> 
					<td width="82%" align="left" class="big mailSubHeader">
<div class="wallbuttons">
				<input type="button" onclick="getWall('Common','10');" value="Commonooo" class="crmbutton small create">
				<input type="button" onclick="getWall('Facebook','10');" value="Facebook" class="crmbutton small create">
				<input type="button" onclick="getWall('Twitter','50');" value="Twitter" class="crmbutton small create">
                                <input type="button" onclick="getWall('Linkedin','50');" value="LinkedIn" class="crmbutton small create">
	</div>	</td>
				</tr>
			</table>-->
<br><!--
<table>
<tr>
<td width="14%" align="left" >
<div id = 'postonsocialmedia' class ='postonsocialmedia'>
<div class="tweetpostdiv" id="tweetpostdiv">	<div class=textareaplusicons><div class=tweetbox>	<textarea id=composetweettxt onkeyup=countcharacters();></textarea></div><div class=tweetaddpic><img src ='modules/SocialFeeds/twitcam.png' id='me'><input type=hidden id=imgfilename></div></div><br><div class=buttonpluscharscount><div class=composetweetbutton><input type=button value=Tweet onclick=composeTweet() class='small create floatright' id=posttweettotwitter></div><div id=charscount class =floatleft>140</div></div></div>
<div class="tweetpostdiv" id="statuspostfbwall"><textarea name=posttextarea class=txtbox floatleft id=posttextarea></textarea><br><input type=button value='Post In FbWall' onclick=postFeed() class='small create floatright' id=posttweettotwitter></div>
<div class="tweetpostdiv" id="postcommonwall"><textarea name='posttextarea' class='txtbox' id='postcommom'></textarea><br><input type="button" value="Post in All" class="small create" onclick=postCommonFeed();></div>
<div class="tweetpostdiv"id = "statuspostlinkedin"><textarea name='posttextarea' class='txtbox' id='postlinkedin'></textarea><br><input type="button" value="Post In LinkedIn" class="small create floatright" onclick='postinLinkedIn();'></div>
</div>
</td>
</tr>
</table>
<br>-->
<div>
			<table class="mailClient" cellspacing="0" cellpadding="0" width="100%" border="0">
			<tr>
			<td width = "45%">
<div class="feedswrapper">
	<div class="stats" id="stats"><div class="insidestats"id = "insidestats"></div></div>
	<div class = "imagebuttons">
                                <a href='javascript:' onclick="getWall('Common','10');" ><img src='themes/images/common_wall.png' alt="Common" /></a>
                                <a href='javascript:' onclick="getWall('Facebook','10');"><img src='themes/images/facebook.png' alt='Facebook' /></a>
                                <a href='javascript:' onclick="getWall('Twitter','50');"><img src='themes/images/twitter.png' alt='Twitter' /></a>
                                <a href='javascript:' onclick="getWall('Linkedin','50');"><img src='themes/images/linkedin.png' alt='LinkedIn' /></a>

	</div>
	<div class = "feedsdiv">
<div id = 'postonsocialmedia' class ='postonsocialmedia'>
<input type="hidden" id="ismediauploading" value = "no">
<!--tweetpostdiv-->
<div class="tweetpostdiv" id="tweetpostdiv">    <div class=textareaplusicons><div class=tweetbox> <textarea id=composetweettxt onkeyup=countcharacters();></textarea></div><div class=tweetaddpic><img src ='modules/SocialFeeds/twitcam.png' id='me'><input type=hidden id=imgfilename> <br> <div id = "loading_tweet" style = "display:none;"> <img src = 'themes/images/loading.gif' alt = 'Loading' title = 'Loading'> </div> </div> </div> <br><div class=buttonpluscharscount><div class=composetweetbutton><input type=button style = "margin-top:8px;" value=Tweet onclick=composeTweet() class='small create floatright' id=posttweettotwitter></div><div id=charscount class =floatleft>140</div></div></div><div id = "compose_message" style = "padding-left:35px;color:green;font-size:15px;display:none"> Posted Successfully </div>
<!--fbwall post-->
<div class="tweetpostdiv" id="statuspostfbwall"><div class=textareaplusicons><div class=tweetbox><textarea name=posttextarea class=txtbox id=posttextarea></textarea></div><div class=tweetaddpic><img src='modules/SocialFeeds/twitcam.png' id='me1'><input type=hidden id=imgfilename1></div><div class='composetweetbutton'><input type='button' style = "margin-top:8px;" value='Post In FbWall' onclick=postFeed() class='small create floatright' id=posttweettotwitter></div> </div>
<div id = 'loading_post' style = 'display:none;float: right;'>  <img src = 'themes/images/loading.gif' alt = 'Loading' title = 'Loading'> </div></div>
<!--common wall post-->
<div class="tweetpostdiv" id="postcommonwall"><textarea name='posttextarea' class='txtbox' id='postcommom'></textarea><br><input type="button" value="Post in All" style = "margin-top:8px;margin-right: 25px;" class="small create floatright" onclick=postCommonFeed();></div>
<div id = "post_message" style = "font-size:10px; color:green; padding:5px;display:none"> Posted Successfully </div>
<!--linked in post-->
<div class="tweetpostdiv"id = "statuspostlinkedin"><textarea name='posttextarea' class='txtbox' id='postlinkedin'></textarea><br><input type="button" value="Post In LinkedIn" class="small create floatright" onclick='postinLinkedIn();' style='margin-top:8px;margin-right: 25px;'></div>
</div>
<br>
		<div class ="feedsdiv1" id = "feedsdiv1"></div>
	</div>
<!--				<div class="feedsdiv" id = "feedsdiv2" ></div>-->
</div>
    <!-- div to display events -rightstats-->    <div class="stats" id="rightstats">
<div id ="getEvents" class="insidestats"><!--<a href="javascript:" onClick = "getEvents();">Get Events </a>-->
<br><br>

<div id='displaybirthday'class='displaybirthday'></div>
</div>

</div><!--exits rightstats -->

<div id=morediv class="floatright"></div></td>
		<!--	<td width="30%"><div class="feedsdiv" id = "feedsdiv3"></div></td>-->
			</tr>
			</table>
</div>
		</td>
	</tr>
</table>
