<?php
/*This file is used to took the backup of the core files which is affected by Product Package*/
error_reporting('E_All');
ini_set('display_errors','On');
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
echo '<html><head><title>Social CrmInstallation Script</title>';
echo '<style type="text/css">@import url("themes/softed/style.css");br { display: block; margin: 2px; }</style>';
echo '</head><body class=small style="font-size: 12px; margin: 2px; padding: 2px;">';
echo '<a href="index.php"><img src="themes/softed/images/vtiger-crm.gif" alt="vtiger CRM" title="vtiger CRM" border=0></a><hr style="height: 1px">';
require_once 'config.inc.php';
require_once 'include/utils/utils.php';
require_once('include/DatabaseUtil.php');
echo('<pre>');
global $root_directory ,$site_URL , $adb;
#Creating the smack installation object
$smackinstallObj = simplexml_load_file("smackproductpackage.xml",'SimpleXMLElement', LIBXML_NOCDATA);
$smackinstallArr = smackconvertobj($smackinstallObj);
$IsCustomFunction = smackconvertobj($smackinstallArr['customfunction']);
$IsCustomFunction = $IsCustomFunction['is_custom'];
$smackmodule     = $smackinstallArr['name'];
$smackmodule_lbl = $smackinstallArr['label'];
$smackmoduleprefix = $smackinstallArr['moduleprefix'];
$Tables_dt	 = smackconvertobj($smackinstallObj->tables);
$smackmodule_ver = $smackinstallArr['version'];
$smackconfigInfo = smackconvertobj($smackinstallArr['configuration']);
$ins_warn_msg_arr= smackconvertobj($smackinstallArr['installation']->msg->warning);
$ins_module_link_arr = smackconvertobj($smackinstallArr['installation']->links->module->$smackmodule->link_detail);
$backuppathInfo = getbackuppath($root_directory ,$smackinstallArr);
#unset the $_REQUEST it the smack_extracted session is set

#Backup path permission 
if($backuppathInfo['is_writable'] == 1 && !isset($_REQUEST['smack_status'])){
	$smackBackupPath = $backuppathInfo['folderpath'][0];
	unset($_REQUEST);
}else{
	$temp_request['request'] = "FolderPermission";
	#get the Folderpermission msg
	$backup_failure = smackconvertobj($smackinstallArr['backup']->msg->warning);
	$temp_request['msg'] = $backup_failure['folderpermission'];
	$tempDetails = array_merge($backuppathInfo , $temp_request);
	$Permission_temp = GetDisplayTemplate($tempDetails);
	echo $Permission_temp;
	
}
#PreCheck whether the module is Installed already
#Creating the tables
$BackupFlag = 0;   #condition to check for the backupname in db
$createTables = CreateTables($smackinstallArr['tables'] ,$smackmoduleprefix);
$upversion = setsmackconfig($smackmoduleprefix ,'version',$smackconfigInfo['version'] );
$files_backup = GetFilesToBack($smackinstallObj);
#Fix for coming back by tring to install before extract the files
session_start();
	#Get the zip name if already backup action is done
	$backuped_zip = getsmackconfig($smackmoduleprefix,'backupname');
	if($backuped_zip){
		$Backupfoldername = $backuped_zip;
	}else{
	$cur_time = time();
	#removing space in module label
	$zip_modulelabel = str_replace(" ","_",$smackmodule_lbl);
	$zip_moduleversion = str_replace(" ","_",$smackmodule_ver);
	$Backupfoldername =  $zip_moduleversion.'_'.$zip_modulelabel.$cur_time.'.'.'zip';
	}
if(!isset($_SESSION['smack_extracted'])){
	$BackupInfo = create_zip($files_backup,$smackBackupPath.$Backupfoldername,$smackinstallObj);
	$BackupFlag = $Backupfoldername;
}else if($_SESSION['smack_extracted'] == 'no' && file_exists($smackBackupPath.$Backupfoldername)){
	unset($_SESSION['smack_extracted']);
	$backup['result'] = 1;
	$BackupInfo = array_merge($files_backup , $backup);
}

#Fix ends here
$noof_files = count($BackupInfo);
if($BackupInfo['result'] == 1 ){
	if($BackupFlag && $BackupInfo['request'] != 'filealreadypresent'){
		$upbackup = setsmackconfig($smackmoduleprefix,'backupname',$BackupFlag);
		$is_backup = setsmackconfig($smackmoduleprefix,"is_backup",$smackconfigInfo['is_backup']);
	}
	$Backupsuccess['request'] = "BackupFilesSuccess";
	$success_backupfiles = smackconvertobj($smackinstallArr['backup']->msg->success);
	$Backupsuccess['msg'] = $success_backupfiles['backup'].$Backupfoldername; 
	$temp_backupfiles = array_merge($BackupInfo,$Backupsuccess);
	$dispFiles = GetDisplayTemplate($temp_backupfiles);
	echo($dispFiles);
}

$is_smackconfigExists = CheckSmackConfig();

#Check again  for the smack_config table
$is_smackconfigExists = CheckSmackConfig();

if($is_smackconfigExists){
	#Prechecking before script action
	$Precheck_dt	 = smackconvertobj($smackinstallObj->precheck);
	$precheck 	= Precheck($Precheck_dt,$root_directory);
	setsmackconfig($moduleprefix,'modulename',$smackinstallObj->name);
	setsmackconfig($moduleprefix,'label',$smackinstallObj->label);
	setsmackconfig($moduleprefix,'version',$smackinstallObj->version);
	setsmackconfig($moduleprefix,'is_tared',1);
	$SelectedValues = GetValuesFromVtTale($Tables_dt['table']);
	$InsertInVtiger = InsertInVtiger($Tables_dt['table'],$SelectedValues);
	$CreateTable = CreateTables($smackinstallObj->tables,$moduleprefix);
}


if($is_installed){
setsmackconfig($moduleprefix,'is_installed',1);		
$is_installed = getsmackconfig($moduleprefix,"is_installed");
        $temp_module['request'] = "AlreadyInstalled";
        $temp_module['msg']     = $modulelabel.$InsSuccesMsg['install'];
        $modulelink             = (array) $moduleDt['link_detail']->link;
        $temp_module['link']    = $modulelink[0];
        $modulelinklabel        = (array) $moduleDt['link_detail']->label;
        $temp_module['linklabel']= $modulelinklabel[0].$modulelabel;
        $display_msg =  GetDisplayTemplate($temp_module);
        echo($display_msg);die;
        }

if($IsCustomFunction){
CustomFunction();
}
/**
* Function - no xml values
**/
function CustomFunction(){
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('Contacts');
$block1 = new Vtiger_Block();
$block1->label = 'Source Information';
$module->addBlock($block1);

$field1 = new Vtiger_Field();
$field1->name = 'sourceid';
$field1->label= 'Source Id';
$field1->table = 'vtiger_contactsubdetails';
$field1->column = 'sourceid';
$field1->columntype = 'VARCHAR(255)';
$field1->uitype = 1;
$field1->displaytype = 1;
$field1->typeofdata = 'V~O';
$block1->addField($field1);

$field2 = new Vtiger_Field();
$field2->name = 'source';
$field2->label= 'Source';
$field2->table = 'vtiger_contactsubdetails';
$field2->column = 'source';
$field2->columntype = 'VARCHAR(255)';
$field2->uitype = 1;
$field2->displaytype = 1;
$field2->typeofdata = 'V~O';
$block1->addField($field2);

}
/**
 *Function Forms the array of FileList that is
 *to Backup before the Installation
 *@param $smackinstallObj
 *return array
 **/
function GetFilesToBack($smackinstallObj){
	#Converting the xmlObject to an array
	$BackupArr = get_object_vars($smackinstallObj->backup->filelist);
	foreach($BackupArr as $file){
		$filelist = $file;
	}

	return $filelist;
}

/**
 *Function Converts the given xmlObject to an array
 *@param xmlObject 
 *@return array
 **/
function smackconvertobj($obj){
	return get_object_vars($obj);
}

/**
 *Function used to create smackconfig table
 *@param TableDetails
**/
function CreateSmackConfigTable($TableDetails){
	global $adb;
	$noofTables = count($TableDetails);
	for($i=0;$i<$noofTables;$i++){
		$tablename = $TableDetails[$i]->name;
		if($tablename == "smack_config"){
			$config_sql = $TableDetails[$i]->sql;
			$create_smackconfig = $adb->pquery($config_sql,array());
			return true;
		}		
	}
}

/**
 *Function check for smack_confiq table
 *@param $adb
**/
function CheckSmackConfig(){
	global $adb;
	$chk_smackconfig_sql = $adb->pquery('SHOW TABLES LIKE "%smack_config%"',array());
	return $chk_smackconfig_sql->_numOfRows;
}
/**
 *Function do a precheck before continuing the action
 *@param Detail array
**/
function Precheck($PreCheckDetail,$root_directory){
	$Files_dt	=	$PreCheckDetail['filetocheck'];
	$no_of_files 	= count($PreCheckDetail['filetocheck']);
	for($i=0;$i<$no_of_files;$i++){
		$check_file = file_exists($root_directory.$Files_dt[$i]->name);
		if(!$check_file){
			DisplayWarning($Files_dt[$i]->msg);	
		}
	}
}


/**
 *Function used to Get the wanted entries from the vtiger Tables
 *@param $TableDetails
**/
function GetValuesFromVtTale($TableDetails){
	global $adb;
        $noofTables = count($TableDetails);
        for($i=0;$i<$noofTables;$i++){
		unset($columnDt);
		$EntryType = $TableDetails[$i]->type;
		if($EntryType == "select"){
			$selectSql = $TableDetails[$i]->sql;
			$columns = (array) $TableDetails[$i]->columns;
			$col_varible = (array) $TableDetails[$i]->variables;
			$noofcolumns = count($columns['column']);
			if($noofcolumns > 1){
				for($cnt=0;$cnt<$noofcolumns;$cnt++){
					$columnDt[$col_varible['name'][$cnt]] =	$columns['column'][$cnt];
				}	
			}else{
				$columnDt[$col_varible['name']] = $columns['column'];
			}
			#Execute the Query
			$result = $adb->pquery($selectSql,array());
			foreach($columnDt as $var => $column){
				$SelectResult[$var] = $adb->query_result($result,0,$column);
			}
		}
	}
			return $SelectResult;
}


/**
 *Function used to insert the values in vtigerTable
 *@param $TableDetails , $SelectedValues
**/
function InsertInVtiger($TableDetails , $SelectedValues){
	global $adb;
        $noofTables = count($TableDetails);
	$cout = 1;
        for($i=0;$i<$noofTables;$i++){
		unset($InsertValues);
		$EntryType = $TableDetails[$i]->type;
		
                if($EntryType == "insert" || $EntryType == "update"){
			$param = (array) $TableDetails[$i]->params;
			if(!is_object($param['param'])){
				$noofparams = count($param['param']);
			}else{
				$noofparams = 1;
			}
		 	for($ii = 0;$ii<$noofparams;$ii++){
				$paramDt = $param['param'][$ii];
				if(is_object($paramDt)){
					$paraname = (array) $paramDt->name;
					if($paraname[0] != "DIRECT"){
						$inc = (array) $paramDt->inc;
						$InsertValues[] = $SelectedValues[$paraname[0]] + $inc[0];
					}else{
						$paraname = (array) $paramDt->varname;
						$insvalue = (array) $paramDt->value;
						$InsertValues[] = $insvalue[0];
					}
				}else{
					$InsertValues[] = $SelectedValues[$paramDt]; 
				}
			}
			$InsertSql = $TableDetails[$i]->sql;
			$insertIt  = $adb->pquery($InsertSql,$InsertValues);
		}

	}

}
/**
 *Function used to display the warning message
 *@param Msg_dt
**/
function DisplayWarning($Msg_dt){

	if(is_object($Msg_dt)){
		$Msg_arr = smackconvertobj($Msg_dt);
	}
	$msg = $Msg_arr['warning'];
	echo("<input type = 'hidden' value = '{$msg}' id = 'msg'>");
	echo "<script type='text/javascript' language'='javascript'>
		var msg = document.getElementById('msg').value;
                var conf = confirm(msg);
        if(conf == true){
                window.location.reload();
        }else{
                window.history.back();
        }
        </script>";
	die;
}


/**
 *Function create and alter the Tables which is used for
 *the Installation and module functionality  
 *@param tablesdetails ,moduleprefix 
**/
function CreateTables($table_details ,$moduleprefix){
	global $adb ;
	$noofTables = count($table_details->table);
	for($i = 0 ; $i < $noofTables ; $i++){
		$tableArr = smackconvertobj($table_details->table[$i]);
		if($tableArr['type'] == 'create'){
			$tablecreated = $adb->pquery($tableArr['sql'],array());
			if($tablecreated){
				if($tableArr['name'] != 'smack_config'){
					setsmackconfig($moduleprefix , "tbl_{$tableArr['name']}" ,'created');
				}
			}
		}else if ($tableArr['type'] == 'alter'){
			$altertable = $adb->pquery($tableArr['sql'],array());
			if($altertable){
                                if($tableArr['name'] != 'smack_config'){
                                        setsmackconfig($moduleprefix , "tbl_{$tableArr['name']}" ,'altered');
                                }
                        }

		}
	}

}
/**
 *Function to get the Backup path for the core files and also check whether it is writable or not
 *@param $root_directory ,$smackinstallObj 
 *return backup path
 **/
function getbackuppath($root_directory,$smackinstallArr){
	$backup_folders = smackconvertobj($smackinstallArr['backup']->havepermission->folderlist);
	$permissionFlag = 0 ;
	if(is_array($backup_folders['foldername'])){
	foreach($backup_folders['foldername'] as $folder){
		$folder_path = $root_directory.$folder.'/';
		if(!is_writeable($folder_path)){
			$permissionFlag = 1;
			$Not_writeable['folderpath'][] = $folder_path;
		}else{
			$backup_path['folderpath'][] = $folder_path;
		}
	}
	}else{
		$folder_path = $root_directory.$backup_folders['foldername'].'/';
		if(!is_writable($backup_folders['foldername'])){
			$permissionFlag = 1;
			$Not_writeable['folderpath'][] = $folder_path;
		}else{
			$backup_path['folderpath'][] = $folder_path;
		}
	}
	if($permissionFlag){
		$Not_writeable['is_writable'] = 0;
		return $Not_writeable;

	}else{
		$backup_path['is_writable'] = 1;
		return $backup_path;
	}
}
/**
 *creates a compressed zip file 
**/
function create_zip($files = array(),$destination = '',$smackinstallObj,$overwrite = false) {
	if(file_exists($destination) && !$overwrite) {
		$backup_files = GetFilesToBack($smackinstallObj); 
		$backup_files['result'] = 1;
		$backup_files['request'] ="filealreadypresent";
		return $backup_files;
	}
	$valid_files = array();
	if(is_array($files)) {
		foreach($files as $file) {
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	if(count($valid_files)) {
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		foreach($valid_files as $file) {
			$zip->addFile($file,$file);
		}
		$zip->close();
		$iszip =  file_exists($destination);
		$iszip_created['result'] = $iszip;	
		$return_data = array_merge($valid_files , $iszip_created);
		return $return_data;
	}
	else
	{
		return false;
	}
}

/**
 *Function is used to get the smack_config values
 *@param $prefix , $option
 *retrun smack_config required values
 **/
function getsmackconfig($prefix , $option){
	global $adb;
	$colname = $prefix.$option;
	$smackconfig_sql = $adb->pquery("select * from smack_config where optionname = ?",array($colname));
	$smackconfig  = $adb->query_result($smackconfig_sql , 0 , 'value');
	if(isset($smackconfig)){
		return $smackconfig; 
	}else{
		return false;
	}
}
/**
 *Function is used to set the smack_config values
 *@param $prefix , $option ,$value
 *
 **/
function setsmackconfig($prefix , $option ,$value){
	global $adb;
	$is_update = getsmackconfig($prefix , $option);
	$colname = $prefix.$option;
	if($is_update){	
		$update_col = $adb->pquery("update smack_config set value = ? where optionnanme = ?" ,array($value ,$colname));
	}else{
		$ins_col = $adb->pquery("insert into smack_config (optionname , value)values(? , ?)",array($colname , $value));
	}
}
/**
 *Function is used to delete the smack_config values
 *@param $prefix , $option ,$value
 *
 **/
function deletesmackconfig($prefix , $option){
        global $adb;
	$colname = $prefix.$option;
	$deleteentry = $adb->pquery("delete from smack_config where optionname = ?",array($colname));
}

/**
 *This function gives the template design for the Given input
 *@param #tempDetails
 *return template
**/
function GetDisplayTemplate($tempDetails){
	if($tempDetails['request'] == "FolderPermission"){
		$content = "<div style = 'background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DFDFDF;border-radius: 3px 3px 3px 3px;color: #333333;font-family: sans-serif;margin: 2em auto;margin-top:50px;min-width: 700px;padding: 1em 2em;'>"; 
		$content .= "<h3>{$tempDetails['msg']}</h3>";
		foreach($tempDetails['folderpath'] as $not_writable ){
			$content .="<p style='font-size: 14px;line-height: 1.5;margin: 25px 0 20px'>{$not_writable}</p>";	
		}
		$content .= '<form name = "smackbackupwarn" action = "smackinstall.php" method ="POST"><input type = "hidden" value ="backup" name="backtest">';
		$content .= '<input type = "submit" value = "continue" style = "    border: 1px solid #BBBBBB;border-radius: 15px 15px 15px 15px;color: #464646;cursor: pointer;    line-height: 16px;padding: 6px 12px;">';
		$content .="</div>";
	}else if($tempDetails['request'] == 'BackupFilesSuccess'){
		$content = "<div style = 'background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DFDFDF;border-radius: 3px 3px 3px 3px;color: #333333;font-family: sans-serif;margin: 2em auto;margin-top:50px;min-width: 700px;padding: 1em 2em;'>";
                $content .= "<h3>{$tempDetails['msg']}</h3>";
		foreach($tempDetails as $detailentity =>$files ){
			if($detailentity != 'result' && $detailentity != 'msg' && $detailentity != 'request'){
                        $content .="<p>{$files}</p>";
			}
                }
                $content .= '<form name = "smackinstall" action = "smackinstall.php" method ="POST"><input type ="hidden" value = "backup_obtained" name ="smack_status"/>';
                $content .= '<input type = "submit" value = "continue" style = "border: 1px solid #BBBBBB;border-radius: 15px 15px 15px 15px;color: #464646;cursor: pointer;    line-height: 16px;padding: 6px 12px;" onclick = "return IsUntared();">';
                $content .="</div>";
		return $content;
	}else if($tempDetails['request'] == "AlreadyInstalled"){
		$content = "<div style = 'background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DFDFDF;border-radius: 3px 3px 3px 3px;color: #333333;font-family: sans-serif;margin: 2em auto;margin-top:50px;min-width: 700px;padding: 1em 2em;'>";
                $content .= "<h3>{$tempDetails['msg']}</h3>";
		$content .="<p><a href ={$tempDetails['link']} style = 'border: 1px solid #BBBBBB;border-radius: 15px 15px 15px 15px;color: #464646;cursor: pointer;    line-height: 16px;padding: 6px 12px;text-decoration:none;' >{$tempDetails['linklabel']}</a></p>";
		$content .="</div>";
		 return $content;

	}

}
function recheckInstall($msg){
echo("<input type = 'hidden' value = '{$msg}' id = 'msg'>");
echo "<script type='text/javascript' language'='javascript'>
		var msg = document.getElementById('msg').value;
                var conf = confirm(msg);
        if(conf == true){
                window.location.reload();
        }else{
                window.location.reload();
        }
        </script>";
	die;
}
?>
