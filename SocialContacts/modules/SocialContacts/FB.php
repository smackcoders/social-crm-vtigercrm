<?php
require_once 'src/facebook.php';
global $adb;
# Getting the Facebook Token, Secret, App Id from SocioSettings Table

$getFacebookIDQuery = $adb->pquery("select facebook_app, facebook_secret, facebook_access from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getFacebookIDQuery);
$fb = true;
if($queryCount != 0)
{
        $facebookapp    = $adb->query_result($getFacebookIDQuery,0,"facebook_app");
        $facebooksecret = $adb->query_result($getFacebookIDQuery,0,"facebook_secret");
        $facebookaccess = $adb->query_result($getFacebookIDQuery,0,"facebook_access");
        if(empty($facebookapp) && empty($facebooksecret) && empty($facebookaccess))
        {
                $fb = false;
        }
}

if($queryCount == 0 || empty($fb) || !isset($fb))
{
        echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'> </td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> Facebook App Key or Secret Key not given in the SocioSettings </strong> </span> </td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
		<a href = 'index.php?module=Settings&action=SocioSettings'> Click Here to go to Settings Page </a>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
        exit;
}

$facebook = new Facebook(array(
      'appId'  => $facebookapp,
      'secret' => $facebooksecret,
    ));

$user     = $facebook->getUser();
$loginurl = $facebook->getLoginUrl(array("scope" => "read_stream,publish_stream,read_mailbox, offline_access,read_page_mailboxes, user_about_me, user_likes,user_website, user_events, user_status, friends_likes, friends_website, friends_events, friends_status, user_photos, friends_photos, friends_birthday"));

if($user)
{
        $acctoken = $facebook->getAccessToken();
        $adb->pquery("update vtiger_sociosettings set facebook_access = ? where id = 1",array($acctoken));
        echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> Facebook Access Key Saved Successfully </strong> </span> </td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
		 <a href = 'index.php?module=Settings&action=SocioSettings'> Click Here to go to Settings Page </a>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
        exit;
}
else{
        echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> <a href = $loginurl > Click Here to Go to App </a> </strong> </span> </td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
	exit;
}

