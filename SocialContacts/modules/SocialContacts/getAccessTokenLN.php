<?php
global $adb;
require_once('linkedin_3.2.0.class.php');

$getLinkedIDQuery = $adb->pquery("select linkedintoken1, linkedintoken2, linkedinauth1, linkedinauth2 from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getLinkedIDQuery);
if($queryCount != 0){
        $token1 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken1");
        $token2 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken2");
        $auth1 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth1");
        $auth2 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth2");
}
  $API_CONFIG = array(
          'appKey'=> $token1,
          'appSecret'=> $token2,
  );

//add callback url

      if($_SERVER['HTTPS'] == 'on') {
        $protocol = 'https';
      } else {
        $protocol = 'http';
      }
      $API_CONFIG['callbackUrl'] = $protocol . '://' . $_SERVER['SERVER_NAME'] . ((($_SERVER['SERVER_PORT'] != PORT_HTTP) || ($_SERVER['SERVER_PORT'] != PORT_HTTP_SSL)) ? ':' . $_SERVER['SERVER_PORT'] : '') . $_SERVER['PHP_SELF'] . '?' . 'module=SocialContacts&action=getAccessTokenLN&gottoken=1';

$OBJ_linkedin = new LinkedIn($API_CONFIG);

//before going to linkedin page
if(!$_REQUEST['gottoken']){
$response = $OBJ_linkedin->retrieveTokenRequest();
	 if($response['success'] === TRUE) {
          $_SESSION['oauth']['linkedin']['request'] = $response['linkedin'];
		
$loginurl = LINKEDIN::_URL_AUTH . $response['linkedin']['oauth_token'];

echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> <a href = $loginurl > Click Here to Go to App </a> </strong> </span> </td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";

        } else {
          echo "Request token retrieval failed - Pls Check LinkedIn Credentials";
        }
}
else{
	$response = $OBJ_linkedin->retrieveTokenAccess($_SESSION['oauth']['linkedin']['request']['oauth_token'], $_SESSION['oauth']['linkedin']['request']['oauth_token_secret'], $_GET['oauth_verifier']);
	if($response['success'] === TRUE) {
	        $_SESSION['oauth']['linkedin']['access'] = $response['linkedin'];
		$adb->pquery("update vtiger_sociosettings set linkedintoken = ?, linkedinsecret = ? where id = 1",array($response['linkedin']['oauth_token'],$response['linkedin']['oauth_token_secret']));
	        $_SESSION['oauth']['linkedin']['authorized'] = TRUE;

        echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> LinkedINAccess Key Saved Successfully </strong> </span> </td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                 <a href = 'index.php?module=Settings&action=SocioSettings'> Click Here to go to Settings Page </a>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
        } else {
          // bad token access
          echo "Access token retrieval failed: Please Check LinkedIn Configuration.";
        }
}
