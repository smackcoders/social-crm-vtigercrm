<?php
require_once('linkedin_3.2.0.class.php');
require_once("modules/SocialContacts/SocialContacts.php");
require_once("modules/SocialHistory/SocialHistory.php");
global $current_user, $currentModule,$adb;
/* get linked in app credentials from db */
$getLinkedIDQuery = $adb->pquery("select linkedinsecret, linkedintoken, linkedintoken1, linkedintoken2, linkedinauth1, linkedinauth2 from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getLinkedIDQuery);
if($queryCount != 0)
{
        $token1 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken1");
        $token2 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken2");
        $auth1 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth1");
        $auth2 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth2");
        $accToken =$adb->query_result($getLinkedIDQuery,0,"linkedintoken");
        $secToken = $adb->query_result($getLinkedIDQuery,0,"linkedinsecret");
}

$focus = new SocialContacts();
$focus1 = new SocialHistory();
/* get conatcts from linkedIn in new method */
  $API_CONFIG = array(
    	  'appKey'=> $token1,
          'appSecret'    => $token2,
          'callbackUrl'  => NULL
  );
  define('CONNECTION_COUNT', 20);
  define('PORT_HTTP', '80');
  define('PORT_HTTP_SSL', '443');
  define('UPDATE_COUNT', 10);

$OBJ_linkedin = new LinkedIn($API_CONFIG);
        $setToken = array('oauth_token' =>$accToken, 'oauth_token_secret' => $secToken);
        $OBJ_linkedin->setTokenAccess($setToken);

$response = $OBJ_linkedin->connections('~/connections');
if($response['success'] === TRUE) {
              $connections = new SimpleXMLElement($response['linkedin']);
	      $ar = $connections->person;
}
else{

echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'><img src='". vtiger_imageurl('denied.gif', $theme) ."' ></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> Check LinkedIn Credentials </strong> </span></td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                <a href='index.php?module=Settings&action=SocioSettings'> Click Here to go Settings Page.</a><br>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
exit;
}
$count = 0;
for($i = 0; $i < count($ar); $i ++)
{
	$facebook_id = $adb->query_result($adb->pquery("select socialcontactsid from vtiger_socialcontacts join vtiger_crmentity on socialcontactsid = crmid and deleted = 0 where socialcontactid=?", array((string) $ar[$i]->id)),0,'socialcontactsid');
	if(!$facebook_id)
	{
		   $focus->column_fields['SocialContactId'] = (string) $ar[$i]->id;
		   $focus->column_fields['SocialContactName'] = (string)$ar[$i]->{'first-name'}.' '.(string)$ar[$i]->{'last-name'};
		   $focus->column_fields['SocialContactWebsite'] =$ar[$i]->{'site-standard-profile-request'}->url;
		   $focus->column_fields['SocialContactEmail'] = ' ';
		   $focus->column_fields['SocialContactLocation'] = (string)$ar[$i]->location->name;
		   $focus->column_fields['SocialContactHometown'] = (string)$ar[$i]->headline;
	           $focus->column_fields['source'] = 'LinkedIn';
		   $focus->column_fields['assigned_user_id'] = 1;
		   $focus->save("SocialContacts"); 

  		   $focus1->column_fields['socialcontactid'] = (string) $ar[$i]->id;
     		   $focus1->column_fields['socialcontactname'] = (string)$ar[$i]->firstName.' '.(string)$ar[$i]->lastName;
    		   $focus1->column_fields['feed'] = 'Contact Saved';
      		   $focus1->column_fields['assigned_user_id'] = 1;
       		   $focus1->column_fields['source']  = 'LinkedIn';
		   $focus1->save('SocialHistory');
		   $count++; 
		}
}
echo "<b style = 'font-size:25px; color:green; padding-left:30%'> $count contacts have been saved from LinkedIn </b>";
