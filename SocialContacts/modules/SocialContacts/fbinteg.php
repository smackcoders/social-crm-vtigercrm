<?php
require_once("config.php");
require_once('include/utils/utils.php');
require_once("modules/SocialContacts/SocialContacts.php");
require_once("modules/SocialHistory/SocialHistory.php");
require 'include/utils/twitterlibrary/tmhOAuth.php';
require 'include/utils/twitterlibrary/tmhUtilities.php';
global $current_user, $currentModule,$adb;

$focus = new SocialContacts();
$focus1 = new SocialHistory();

# Getting the Twitter id from SocioSettings Table
$getTwitterIDQuery = $adb->pquery("select twitter, twitter_usecret, twitter_utoken, twitter_csecret, twitter_ckey from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getTwitterIDQuery);

if($queryCount != 0)
{
        $usecret = $adb->query_result($getTwitterIDQuery,0,"twitter_usecret");
        $utoken = $adb->query_result($getTwitterIDQuery,0,"twitter_utoken");
        $csecret = $adb->query_result($getTwitterIDQuery,0,"twitter_csecret");
        $ckey = $adb->query_result($getTwitterIDQuery,0,"twitter_ckey");
	$twitterid = $adb->query_result($getTwitterIDQuery,0,"twitter");
}

if($queryCount == 0)
{
        echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'><img src='". vtiger_imageurl('denied.gif', $theme) ."' ></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> Check Twitter Credentials.</strong> </span></td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                <a href='index.php?module=Settings&action=SocioSettings'> Click Here to go Settings Page.</a><br>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
        exit;

}

/*      $trends_url = "http:/i/api.twitter.com/1/statuses/friends/{$twitterid}.json";
      $ch = curl_init(); 
      curl_setopt($ch, CURLOPT_URL, $trends_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $curlout = curl_exec($ch);
      curl_close($ch);
      $response = json_decode($curlout, true);*/
      $count = 0;
$tmhOAuth = new tmhOAuth(array(
  'consumer_key'    => $ckey,
  'consumer_secret' => $csecret,
  'user_token'      => $utoken,
  'user_secret'     => $usecret,
));
$code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/followers/list', 'json'),array(
  'screen_name' => $twitterid));
  if ($tmhOAuth->response['code'] == 200) {
    $response = json_decode($tmhOAuth->response['response'], true);
}

      foreach($response['users'] as $friends)
      {
	    $twitt_id = $adb->query_result($adb->pquery("select socialcontactsid from vtiger_socialcontacts join vtiger_crmentity on socialcontactsid = crmid and deleted = 0 where socialcontactid=?", array($friends['id'])),0,'socialcontactsid');
if(!$twitt_id)
{
  	$url = $friends['screen_name'];
	$focus->column_fields['SocialContactId'] = $friends['id'];
	$focus->column_fields['SocialContactName'] = $friends['name'];
	$focus->column_fields['SocialContactEmail'] = '';
	$focus->column_fields['SocialContactLocation'] = $friends['time_zone'];
	$focus->column_fields['SocialContactHometown'] = '';
	$focus->column_fields['SocialContactWebsite'] = "http://www.twitter.com/{$url}";
	$focus->column_fields['source'] = 'Twitter';
	$focus->column_fields['assigned_user_id'] = 1;
	$focus->save("SocialContacts");
        $focus1->column_fields['socialcontactid'] = $friends['id'];
        $focus1->column_fields['socialcontactname'] = $friends['name'];     
	$focus1->column_fields['feed'] = 'Contact Saved';
        $focus1->column_fields['assigned_user_id'] = 1;
        $focus1->column_fields['source']  = 'Twitter';
	$focus1->save('SocialHistory');

$count++; 
}
     } 

echo "<center><h2>".$count." Contacts have been saved from Twitter </h2></center>";


?>

