<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

$mod_strings = Array(
	'ModuleName' => 'Module Name',
	'SINGLE_ModuleName' => 'Module Name',
	'ModuleName ID' => 'Module Name ID',

	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',
	'SINGLE_SocialContacts' => 'Social Contacts',
	'SOCIAL_CONTACT_INFORMATION' => 'SOCIAL CONTACT INFORMATION',
	'SocialContactEmail' => 'Social Contact Email',
	'SocialContactName' => 'Social Contact Name',
	'SocialContactLocation' => 'Social Contact Location',
	'SocialContactWebsite' => 'Social Contact Website',
	'SocialContactHometown' => 'Social Contact Hometown',
	'SocialContacts' => 'Social Contacts',
	'SocialContactId' => 'Social Contact Id',
);

?>
