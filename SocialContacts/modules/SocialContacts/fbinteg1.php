<?php
require 'src/facebook.php';
require_once("modules/SocialContacts/SocialContacts.php");
require_once("modules/SocialHistory/SocialHistory.php");
global $current_user, $currentModule,$adb;

$focus = new SocialContacts();
$focus1 = new SocialHistory();

# Getting the Facebook Token, Secret, App Id from SocioSettings Table

$getFacebookIDQuery = $adb->pquery("select facebook_app, facebook_secret, facebook_access from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getFacebookIDQuery);
$fb = true;
if($queryCount != 0)
{
        $facebookapp = $adb->query_result($getFacebookIDQuery,0,"facebook_app");
        $facebooksecret = $adb->query_result($getFacebookIDQuery,0,"facebook_secret");
        $facebookaccess = $adb->query_result($getFacebookIDQuery,0,"facebook_access");
	if(empty($facebookapp) && empty($facebooksecret) && empty($facebookaccess))
	{
		$fb = false;
	}
}

if($queryCount == 0 || empty($fb) || !isset($fb))
{
        echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'><img src='". vtiger_imageurl('denied.gif', $theme) ."' ></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> Check Facebook Credentials. </strong> </span> </td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                <a href='index.php?module=Settings&action=SocioSettings'> Click Here to go Settings Page. </a><br>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
        exit;
}

    $facebook = new Facebook(array(
      'appId'  => $facebookapp,
      'secret' => $facebooksecret,
    ));
    $facebook->setAccessToken($facebookaccess);
    $user = $facebook->getUser();

$count = 0;
if($user)
{
try 
{
        $friends = $facebook->api('/me/friends');
        foreach ($friends["data"] as $value) 
	{
	    	$facebook_id = $adb->query_result($adb->pquery("select socialcontactsid from vtiger_socialcontacts join vtiger_crmentity on socialcontactsid = crmid and deleted = 0 where socialcontactid=?", array($value["id"])),0,'socialcontactsid');
	if(!$facebook_id)
	{
		   //get fb username of friends
	       	   $frdid = $value['id'];
		   $frd = $facebook->api("/$frdid");
		   $frdemail =$frd['username']."@facebook.com"; 
		   $url = "www.facebook.com/".$value["id"];
		   $focus->column_fields['SocialContactId'] = $value["id"];
		   $focus->column_fields['SocialContactName'] = $value['name'];
		   $focus->column_fields['SocialContactWebsite'] =$url;
		   $focus->column_fields['SocialContactEmail'] = $frdemail;
	 	   $focus->column_fields['source'] = 'Facebook';
		   $focus->column_fields['assigned_user_id'] = 1;
		   $focus->save("SocialContacts"); 
  		   $focus1->column_fields['socialcontactid'] = $value['id'];
     		   $focus1->column_fields['socialcontactname'] = $value['name'];
    		   $focus1->column_fields['feed'] = 'Contact Saved';
      		   $focus1->column_fields['assigned_user_id'] = 1;
       		   $focus1->column_fields['source']  = 'Facebook';
		   $focus1->save('SocialHistory');
		   $count++; 
		}
           }
     } 
     catch (FacebookApiException $e) 
     {
      	  error_log($e);
          $user = null;
     }
}
else
{
	$loginUrl = $facebook->getLoginUrl();
	echo "<center><h2><a href = $loginUrl target=_blank>Login In Facebook</a></h2></center>";
}
echo "<center><h2>".$count." contacts have been saved from Facebook.";
echo "<a href = 'index.php?action=ListView&module=SocialContacts'><u style = 'padding-left:20px;'>Return back to module<u></a></h2></center>";





