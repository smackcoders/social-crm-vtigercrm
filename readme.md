1. Unzip SocialCRM.zip from vtigercrm root directory which has three files.(smackinstall.php, smackproductpackage.xml and vtigercrm_files.tar.gz)

2.Execute smackinstall.php. (http://localhost/vtigercrm/smackinstall.php)

3.Import the Modules present in the zip. (SocialContacts, SocialFeeds, SocialHistory)

4.Untar vtigercrm_files.tar.gz

5. Follow the below steps to get the id and save it on SocioSettings in Vtigercrm Settings module

How to import module in vtigercrm ?
-----------------------------------
	1. Login into vtigerCrm.
	2. Go to CRM Settings present at the top right corner.
	3. Click Module Manager.
	4. Click Custom Module.
	5. Click Import New. Then Choose the zip file and click Import.

TWITTER:
--------

How to get Twitter username ?

	1. Login to Twitter.
	2. Go to Settings.
	3. There you can find the username.

User should uncheck the "Protect my Tweets" checkbox in Twitter Settings.

Go to https://dev.twitter.com/apps/new to create consumer key, consumer secret, user token, user secret

Fill all the fields and create a new application.

After you created the app, It will show you the key in Detailsss tab. Go to Settings tab. Change the application type from Read only to Read, Write and Access direct messages

FACEBOOK:
---------

How to get the AppID, Secret Key from Facebook ?

1. Go to http://developers.facebook.com/
2. Click Apps
3. Click Create New App. You can see that at the top right corner.
4. After that, a pop will be shown. You have to give App name. Then Click Continue.
5. Then you will be redirected to a page that contains the App ID, App Secret. You have to enter this App ID, App Secret in      SocioSettings in vtigercrm.
6. Then you have to enter your domain name in Site URL. You can find this by clicking "Website with Facebook Login".


LINKEDIN:
---------

1. Go to http://developer.linkedin.com/
2. Go to Support -> API Keys
3. Click Add new Application link.
4. Fill all the required fields and click Add Application.
5. You wil get the API Key, Secret Key, OAuth User Token, OAuth User Secret.

ChangeLog:
----------
1.2.5   - Added oauth support for linkedin api
1.2.0   - Upgraded to twitter api version 1.1.
1.1.0   - Performance Increase, added twitter direct message feature and linkedIn.
1.0.0   - Stable version inital release.



