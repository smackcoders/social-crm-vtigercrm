<?php
require_once('include/logging.php');
$writelog =& LoggerManager::getLogger('SOCIALCRM');
$tweettxt= $_REQUEST['tweettxt'];
$potopath = $_REQUEST['potopath'];
global $root_directory;
global $tmp_dir;
require_once("TwitterConfig.php");
if((isset($potopath))&&($potopath!=' '))
{
	$imagepath = $root_directory.$tmp_dir.$potopath;
	include('resizeclass.php');
	$image = new SimpleImage();
	$image->load($imagepath);
	$actualwidth = $image->getWidth();
	$actualheight = $image->getHeight();
	$aspectratio = $actualwidth/$actualheight;
	$resizedwidth = 1024;
	$resizedheight = $resizedwidth/$aspectratio;
	$image->resize(1024,$resizedheight);
	$resizedimage = $root_directory.$tmp_dir.$potopath;
	$image->save($resizedimage);
	$imagepath = $resizedimage;
	$code = $tmhOAuth->request(
			'POST',
			'https://api.twitter.com/1.1/statuses/update_with_media.json ',
			array(
				'media[]'  => "@{$imagepath};type=image/jpeg;filename={$imagepath}",
				'status'   => $tweettxt,
			     ),
			true, // use auth
			true  // multipart
			);
}
else
{
	$code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update'), array(
				'status' => $tweettxt 
				));
}

if ($code == 200) {
	echo "success";
} else {
	$writelog->fatal("Exception while updating twitter - Please Check Twitter Configuration");
	$writelog->fatal("Posting Tweet Fails due to error code - ".$code);
	echo "failure";
}
