<?php
require_once('FbConfig.php');
require_once("modules/SocialHistory/SocialHistory.php");

require_once('include/logging.php');
$writelog =& LoggerManager::getLogger('SOCIALCRM');

global $current_user, $currentModule,$adb;

$focus1 = new SocialHistory();
$likeid =$_REQUEST['likeid'];
$fromname = $_REQUEST['fromname'];
$posttype= $_REQUEST['posttype'];

try
{
	$user_id = $facebook->getUser();
	$likeid = trim($likeid);

	$ee = $facebook->api("/$likeid/likes", 'delete');
	$focus1->column_fields['socialcontactid'] = '';//if needed save likeid here
     	$focus1->column_fields['socialcontactname'] = $fromname;
    	$focus1->column_fields['feed'] = $posttype.' Disliked';
      	$focus1->column_fields['assigned_user_id'] = 1;
       	$focus1->column_fields['source']  = 'Facebook';
	$focus1->save('SocialHistory');
	echo 'Posted Sucessfully '.$ee;
}
catch (FacebookApiException $e) 
{
        error_log($e);
        $user = null;
	$writelog->fatal("Exception from Facebook. Please Check Facebook Configuration");
	$writelog->fatal($e->getMessage());
}
