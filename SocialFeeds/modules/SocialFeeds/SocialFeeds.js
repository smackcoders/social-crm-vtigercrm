/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
function func1() 
{
	document.getElementById('status').style.display = 'inline';
	getWall('Common',150);
}
window.onload=func1;

function getWall(socialmedia,limit)
{
	if(socialmedia=='Facebook')
	{
		document.getElementById('status').style.display = 'inline';

		new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=getwall&socialmedia='+socialmedia+'&limit='+limit,
			onComplete: function(response)
			{
				document.getElementById('feedsdiv1').innerHTML = response.responseText;
				document.getElementById('status').style.display = 'none';
				document.getElementById('postcommonwall').style.display = "none";
				document.getElementById('tweetpostdiv').style.display = 'none';
				document.getElementById('statuspostlinkedin').style.display= 'none';
				if(document.getElementById('iserror').value != 'yes')
				document.getElementById('statuspostfbwall').style.display = "block";
							}
		});
	}
	else if(socialmedia=='Twitter')
	{
		document.getElementById('status').style.display = 'inline';
		new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=GetWallTwitter&socialmedia='+socialmedia+'&limit='+limit,
			onComplete: function(response)
			{
				var n=response.responseText.split("$|||");
				document.getElementById('feedsdiv1').innerHTML = n[0];
				document.getElementById('feedsdiv1').innerHTML += n[1];
				document.getElementById('status').style.display = 'none';
				document.getElementById('postcommonwall').style.display = "none";
				document.getElementById('statuspostfbwall').style.display = "none";
				document.getElementById('statuspostlinkedin').style.display= 'none';
		
				if(document.getElementById('tweeterror').value != 'yes')
		                document.getElementById('tweetpostdiv').style.display = 'block';
				
			}
		});

		new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=Stats',
			onComplete: function(response){
//				document.getElementById('insidestats').innerHTML = response.responseText;
			}
		});
	}//exits twitter
	else if(socialmedia=='Common')
	{
		document.getElementById('statuspostfbwall').style.display = "none";
		document.getElementById('tweetpostdiv').style.display = 'none';
		document.getElementById('status').style.display = 'inline';
		document.getElementById('statuspostlinkedin').style.display= 'none';
		document.getElementById('postcommonwall').style.display = "block";
		//display tweets in common wall
		document.getElementById('status').style.display = 'inline';
		new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=getCommonWall',
			onComplete: function(response)
			{
				document.getElementById('feedsdiv1').innerHTML = response.responseText;
				document.getElementById('status').style.display = 'none';
			}
		});	
	}//exits common wall
	else if(socialmedia=="Linkedin")
	{
		document.getElementById('status').style.display = 'inline';
		new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=GetWallLinkedIn',
			onComplete: function(response)
			{
				document.getElementById('feedsdiv1').innerHTML = response.responseText;
				document.getElementById('postcommonwall').style.display = "none";
				document.getElementById('statuspostfbwall').style.display = "none";
				document.getElementById('tweetpostdiv').style.display = 'none';
				document.getElementById('statuspostlinkedin').style.display= 'block';

				document.getElementById('status').style.display = 'none';
			}
		});
	}
}

//function to like a feed 
function likeFeed(likeid,fromname,posttype)
{
	document.getElementById("loading_"+likeid).style.display = "block";	
	new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=likefeed&likeid='+likeid+'&fromname='+fromname+'&posttype='+posttype,
			onComplete: function(response)
			{
				document.getElementById("loading_"+likeid).style.display = "none";
				document.getElementById('like'+likeid).innerHTML = "<a onclick=dislikeFeed('"+likeid+"','"+fromname+"','"+posttype+"');>Liked</a>";
			}			
	});
}

//to dislike a feed
function dislikeFeed(likeid,fromname,posttype)
{
		document.getElementById("loading_"+likeid).style.display = "block";
		new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=dislikefeed&likeid='+likeid+'&fromname='+fromname+'&posttype='+posttype,
			onComplete: function(response)
			{
				document.getElementById("loading_"+likeid).style.display = "none";
				document.getElementById('like'+likeid).innerHTML = "<a onclick=likeFeed('"+likeid+"','"+fromname+"','"+posttype+"');>Like</a>";
			}		
		});
}

//add comment to feed
function commentOnFeed(postid)
{
	document.getElementById("loading_"+postid).style.display = "block";
	posttext = document.getElementById('txt'+postid).value;
	new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=postcomment&postid='+postid+'&posttext='+posttext,
			onComplete: function(response)
			{
				// Showing the message after commented. Code added by goku
				document.getElementById("loading_"+postid).style.display = "none";
				document.getElementById(postid).innerHTML = response.responseText;
			}
	});
}

function commentDiv(divid)
{
	document.getElementById(divid).style.display = "block";
}

//send message
function showFbDiv()
{
	document.getElementById('tweetdiv').style.display = "none";
	document.getElementById('fbdiv').style.display = "block";
}

function sendfbMsg()
{
//if any problem persists add <!--<div id="fb-root"></div>-->
/*FB.init({appId: '359323720820727', xfbml: true, cookie: true});
      FB.ui({
          method: 'send',
          name: 'Beyond Innovation',
          link: 'http://www.smackcoders.com',
          });
*/
}

//post on my wall
function postFbWall()
{
	document.getElementById('postdiv').style.display = "block";
	document.getElementById("postid").value = 'me';
}

function postFeed()
{
        if(document.getElementById('ismediauploading').value=="yes"){
		alert("Image is Uploading.Please Wait");
		return false;
	}

	var posttext = document.getElementById("posttextarea").value;
	if(posttext.trim() == '') { alert('Please Enter values to post'); return false; }
	document.getElementById('loading_post').style.display = "block";

	var potopath1 = document.getElementById("imgfilename1").value;
	new Ajax.Request(
		'index.php',
		{queue: {position: 'end', scope: 'command'},
		method: 'post',
		postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=postwall&posttext='+posttext+'&potopath='+potopath1,
		onComplete: function(response)
		{
			document.getElementById('loading_post').style.display = "none";
			if(response.responseText == 'success')
			alert("Posted Successfully");
			else
			alert("Check Facebook Configuration");
			document.getElementById("posttextarea").value='';
			document.getElementById("imgfilename1").value='';
		}
	});
}

function importContactsFb()
{
		new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=importcontactFb',
			onComplete: function(response){ }
		});
}

function showTweet()
{
	document.getElementById('fbdiv').style.display = "none";
	document.getElementById('tweetdiv').style.display = "block";
}

function postTweet()
{
	document.getElementById('composetweet').style.display = 'block';
}

function showReplyTweet(statusid)
{
	document.getElementById(statusid).style.display="block";
}

function replyTweet(statusid, userid)
{
	document.getElementById("loading_"+statusid).style.display = "block";
	var replymsg = document.getElementById('txt'+statusid).value;
	new Ajax.Request(
		'index.php',
		{queue: {position: 'end', scope: 'command'},
		method: 'post',
		postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=ReplyTweet&replymsg='+replymsg+'&statusid='+statusid+'&screen_name='+userid,
		onComplete: function(response){  document.getElementById("comment_"+statusid).style.display = "block";
						 document.getElementById("loading_"+statusid).style.display = "none"; }
	});
}

function composeTweet()
{
        if(document.getElementById('ismediauploading').value=="yes"){
                alert("Image is Uploading.Please Wait");
                return false;
        }
	var tweettxt =document.getElementById('composetweettxt').value;
	var potopath =document.getElementById('imgfilename').value;
		document.getElementById('loading_tweet').style.display = "block";
	new Ajax.Request(
		'index.php',
		{queue: {position: 'end', scope: 'command'},
		method: 'post',
		postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=PostTweet&tweettxt='+tweettxt+'&potopath='+potopath,
		onComplete: function(response)
		{
			if(response.responseText == 'success')
				alert("Tweeted Successfully");
			else
				alert("Check Twitter Configuration");
			document.getElementById('composetweettxt').value = ' ';
			document.getElementById('imgfilename').value = ' ';
//			document.getElementById('compose_message').style.display = "block";
			document.getElementById('loading_tweet').style.display = "none";

		}
	});
}

function retweet(statusid)
{
	document.getElementById("loading_"+statusid).style.display = "block";
	new Ajax.Request(
        	'index.php',
                {queue: {position: 'end', scope: 'command'},
                 method: 'post',
                 postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=Retweet&statusid='+statusid,
                 onComplete: function(response)
		{
		 	 document.getElementById('retweet'+statusid).innerHTML = '<img src = modules/SocialFeeds/retweeted.png />Retweeted';
			 document.getElementById("loading_"+statusid).style.display = "none";
			 document.getElementById("comment_"+statusid).style.display = "block";
		}
	});
}

function addFavourite(addfavstatusid)
{
		document.getElementById("loading_"+addfavstatusid).style.display = "block";
		new Ajax.Request(
                        'index.php',
                        {queue: {position: 'end', scope: 'command'},
                        method: 'post',
                        postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=AddFavorite&statusid='+addfavstatusid,
                        onComplete: function(response){
				document.getElementById('fav'+addfavstatusid).innerHTML = "<a onclick = removeFavorite('"+addfavstatusid+"');><img src = modules/SocialFeeds/favorited.png />Favorited</a>";
				 document.getElementById("loading_"+addfavstatusid).style.display = "none";
			}
		});
}

function removeFavorite(statusesid)
{
	document.getElementById("loading_"+statusesid).style.display = "block";
	new Ajax.Request(
        	'index.php',
                {queue: {position: 'end', scope: 'command'},
                method: 'post',
                postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=RemoveFavorite&statusid='+statusesid,
                onComplete: function(response)
		{
			document.getElementById('fav'+statusesid).innerHTML = "<a onclick = addFavourite('"+statusesid+"')><img src = modules/SocialFeeds/favorite.png />Favorite</a>";
			document.getElementById("loading_"+statusesid).style.display = "none";
		}
	});
}

//counts characters and displays remaining characters
function countcharacters()
{
	var chars = (document.getElementById('composetweettxt').value);
	var remainingchars;
	remainingchars = 140-chars.length;
	document.getElementById('charscount').innerHTML =140-chars.length;
	if(remainingchars==0||remainingchars<0)
	{
		document.getElementById("posttweettotwitter").disabled=true;
		document.getElementById("charscount").style.color = 'red';
		document.getElementById("posttweettotwitter").className = 'small floatright';
	}
	else
	{
		document.getElementById("posttweettotwitter").className = 'small create floatright';
		document.getElementById("posttweettotwitter").disabled=false;
		document.getElementById("charscount").style.color = '#000000';
	}
}

function showuserdesc(profstatusid)
{
	document.getElementById('foll'+profstatusid).style.display='block';
	document.getElementById('foll'+profstatusid).style.visibility = 'visible';
}

//works for common wall starts
function postCommonFeed()
{
	var commonposttext;
	commonposttext = document.getElementById('postcommom').value;
	var returnText = null;
	//to post in fbwall
	new Ajax.Request(
		'index.php',
		{queue: {position: 'end', scope: 'command'},
		method: 'post',
		postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=postwall&posttext='+commonposttext,
		onComplete: function(response){ 
			if(response.responseText == 'success'){
				if(returnText != null)
					returnText += ', Facebook';
				else
					returnText = 'Facebook';
			}
        //to post in twitter wall
		        new Ajax.Request(
                	        'index.php',
                        	{queue: {position: 'end', scope: 'command'},
	                        method: 'post',
        	                postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=PostTweet&tweettxt='+commonposttext,
                	        onComplete: function(response){
	                        	if(response.responseText == 'success'){
        	                        	if(returnText != null)
                	                        	returnText += ', Twitter';
	                	                else
        	                	                returnText = 'Twitter';
					}
        				new Ajax.Request(
			                        'index.php',
                        			{queue: {position: 'end', scope: 'command'},
			                        method: 'post',
                        			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=PostLinkedIn&texttopost='+commonposttext,
			                        onComplete: function(response){
                        			if(response.responseText == 'success')
			                                if(returnText != null)
                        			                returnText += ', LinkedIn';
			                                else
                        			                returnText = 'LinkedIn';
						 	alert("Posted in " + returnText);
		                       		 }	
        				});
                	        }
        		});

		}
	});
	document.getElementById("postcommom").value='';
}
function getfbprofsummary(getinfoid,showfeedid)
{
	document.getElementById('prof'+showfeedid).style.display='block';
	document.getElementById('prof'+showfeedid).style.visibility = 'visible';
	document.getElementById('profinfo'+showfeedid).innerHTML = document.getElementById('status').innerHTML;
	new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=FbUserInfo&getinfoid='+getinfoid,
			onComplete: function(response)
			{
				document.getElementById('profinfo'+showfeedid).innerHTML = response.responseText;
			}
	});
}

function showuserdescLinkedin(proflinkedin,profurl)
{
	document.getElementById('prof'+proflinkedin).style.display='block';
	document.getElementById('prof'+proflinkedin).style.visibility = 'visible';
	document.getElementById('profinfo'+proflinkedin).innerHTML = document.getElementById('status').innerHTML;
	new Ajax.Request(
			'index.php',
			{queue: {position: 'end', scope: 'command'},
			method: 'post',
			postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=LinkInUserInfo&geturl='+profurl,
			onComplete: function(response)
			{
				document.getElementById('profinfo'+proflinkedin).innerHTML = response.responseText;
			}
	});
}

function postinLinkedIn()
{
	//has to be modified for common post
	var texttopost = document.getElementById('postlinkedin').value;
	new Ajax.Request(
                        'index.php',
                        {queue: {position: 'end', scope: 'command'},
                        method: 'post',
                        postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=PostLinkedIn&texttopost='+texttopost,
                        onComplete: function(response){ 
				if(response.responseText == 'success')
					alert("Post Updated Successfully");
				else
					alert("Check LinkedIn Configuration");
			}
	});
}

function showSendMsgLink(divid)
{
	document.getElementById(divid).style.display = 'block';
}

function sendmessagelinked(i,sid,updatekey)
{
	var texttosend = document.getElementById('txt'+i+sid).value;
		new Ajax.Request(
                        'index.php',
                        {queue: {position: 'end', scope: 'command'},
                        method: 'post',
                        postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=commentLinkedIn&texttosend='+texttosend+'&sid='+sid+'&updatekey='+updatekey,
                        onComplete: function(response)
			{
				if(response.responseText == 'success')
				alert('Sucessfully Commented');
				else
				alert("Check LinkedIn Configuration");
			}			
		});
}

function dislikelinkedin(lid,i)
{
	new Ajax.Request(
                        'index.php',
                        {queue: {position: 'end', scope: 'command'},
                        method: 'post',
                        postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=dislikeLinkedIn&lid='+lid+'&lik='+'false',
                        onComplete: function(response)
			{
				if(response.responseText == 'success'){
					alert('Sucessfully DisLiked');
					document.getElementById('like'+i+lid).innerHTML = "<a onclick=likelinkedin('"+lid+"','"+i+"')>Like</a>";
				}
				else
					alert("Check Linkedin Configuration");
			}
	});
}

function likelinkedin(lid1,j)
{
		new Ajax.Request(
                        'index.php',
                        {queue: {position: 'end', scope: 'command'},
                        method: 'post',
                        postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=dislikeLinkedIn&lid='+lid1+'&lik='+'true',
                        onComplete: function(response)
			{
				if(response.responseText == 'success'){
					alert('Sucessfully Liked');
					document.getElementById('like'+j+lid1).innerHTML = "<a onclick=dislikelinkedin('"+lid1+"','"+j+"')>Liked</a>";
				}
				else
					alert("Check Linkedin Configuration");
			}
		});
}

function getBirthday()
{
	new Ajax.Request(
                        'index.php',
                        {queue: {position: 'end', scope: 'command'},
                        method: 'post',
                        postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=getBirthdays',
                        onComplete: function(response){}
	});

}

function getEvents(){
}

function getBirthdays()
{
	document.getElementById('displaybirthday').innerHTML = "<br> Loading";
	new Ajax.Request(
                        'index.php',
                        {queue: {position: 'end', scope: 'command'},
                        method: 'post',
                        postBody:'module=SocialFeeds&action=SocialFeedsAjax&file=getBirthdays',
                        onComplete: function(response)
			{
				document.getElementById('displaybirthday').innerHTML = response.responseText;
			}
	});
}
