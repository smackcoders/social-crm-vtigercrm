<?php
require_once('linkedin_3.2.0.class.php');
global $adb;
require_once('include/logging.php');
$writelog =& LoggerManager::getLogger('SOCIALCRM');
        
$getLinkedIDQuery = $adb->pquery("select linkedinsecret, linkedintoken,linkedintoken1, linkedintoken2, linkedinauth1, linkedinauth2 from vtiger_sociosettings where id = 1",array());       
$queryCount = $adb->num_rows($getLinkedIDQuery);
                
if($queryCount != 0){
        $token1 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken1");
        $token2 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken2");
        $auth1 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth1");
        $auth2 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth2");
        $accToken =$adb->query_result($getLinkedIDQuery,0,"linkedintoken");
        $secToken = $adb->query_result($getLinkedIDQuery,0,"linkedinsecret");
}

$API_CONFIG = array(
          'appKey'=> $token1,
          'appSecret'    => $token2,
          'callbackUrl'  => NULL
  );

        $OBJ_linkedin = new LinkedIn($API_CONFIG);
        $setToken = array('oauth_token' =>$accToken, 'oauth_token_secret' => $secToken);
        $OBJ_linkedin->setTokenAccess($setToken);

	$upkey = $_REQUEST['lid'];
	$tru = $_REQUEST['lik'];
	if($tru)
		$response = $OBJ_linkedin->like($upkey);
	else
		$response = $OBJ_linkedin->unlike($upkey);
	if($response['success'] === TRUE) 
		echo "success";
	else{
		echo 'failure';
		$writelog->fatal("Exception in Linkedin. Please Check Linkedin Configuration");
	}
