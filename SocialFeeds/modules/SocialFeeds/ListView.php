<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

require_once('Smarty_setup.php');
require_once("data/Tracker.php");
require_once('include/logging.php');
require_once('include/utils/utils.php');
require_once('modules/CustomView/CustomView.php');
global $current_user;
global $app_strings;
global $mod_strings;
global $currentModule;
global $image_path;
global $theme;
$smarty->assign("theme", $theme);
if ($_REQUEST['ajax'] != '')
	$smarty->display("EmailFeeds.tpl");
else
	$smarty->display("SocialFeeds.tpl");

?>
