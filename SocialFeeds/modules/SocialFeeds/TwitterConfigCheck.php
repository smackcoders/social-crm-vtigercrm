<?php
require 'include/utils/twitterlibrary/tmhOAuth.php';
require 'include/utils/twitterlibrary/tmhUtilities.php';

# Getting the Twitter id from SocioSettings Table
$twitConfig = true;
$getTwitterIDQuery = $adb->pquery("select twitter_usecret, twitter_utoken, twitter_csecret, twitter_ckey from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getTwitterIDQuery);
if($queryCount != 0)
{
        $usecret = $adb->query_result($getTwitterIDQuery,0,"twitter_usecret");
        $utoken = $adb->query_result($getTwitterIDQuery,0,"twitter_utoken");
        $csecret = $adb->query_result($getTwitterIDQuery,0,"twitter_csecret");
        $ckey = $adb->query_result($getTwitterIDQuery,0,"twitter_ckey");
}
if($queryCount == 0 || $utoken == '' || $csecret == '' || $ckey == '')
{
$twitConfig = false;
}
$tmhOAuth = new tmhOAuth(array(
  'consumer_key'    => $ckey,
  'consumer_secret' => $csecret,
  'user_token'      => $utoken,
  'user_secret'     => $usecret,
));

