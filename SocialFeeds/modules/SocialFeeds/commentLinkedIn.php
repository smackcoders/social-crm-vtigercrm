<?php
require_once('linkedin_3.2.0.class.php');
global $adb;

$getLinkedIDQuery = $adb->pquery("select linkedintoken1, linkedinsecret, linkedintoken, linkedintoken2, linkedinauth1, linkedinauth2 from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getLinkedIDQuery);

if($queryCount != 0){
        $token1 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken1");
        $token2 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken2");
        $auth1 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth1");
        $auth2 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth2");
        $accToken =$adb->query_result($getLinkedIDQuery,0,"linkedintoken");
        $secToken = $adb->query_result($getLinkedIDQuery,0,"linkedinsecret");

}
$API_CONFIG = array(
          'appKey'=> $token1,
          'appSecret'    => $token2,
          'callbackUrl'  => NULL
  );

	$OBJ_linkedin = new LinkedIn($API_CONFIG);
        $setToken = array('oauth_token' =>$accToken, 'oauth_token_secret' => $secToken);
        $OBJ_linkedin->setTokenAccess($setToken);

	$response = $OBJ_linkedin->comment($_REQUEST['updatekey'], $_REQUEST['texttosend']);
        if($response['success'] === TRUE) {
		echo 'success';
        }
	else
		echo 'failure'; 
