<?php
require_once 'FbConfigCheck.php';
require_once('include/logging.php');
$noConfiguration = array();
$writelog =& LoggerManager::getLogger('SOCIALCRM');
global $fbConfig;
if($fbConfig){
	try{
		$friends = $facebook->api('/me/home',array('limit'=>'15'));
		for($i=0;$i<count($friends['data']);$i++)
		{
			$fbarray[$i]['source'] = 'fb';
			$fbarray[$i]['updated_time'] = strtotime($friends['data'][$i]['updated_time']);
			$fbarray[$i]['from_name'] = $friends['data'][$i]['from']['name'];
			$fbarray[$i]['feedid'] = $friends['data'][$i]['id'];
			if($friends['data'][$i]['status_type']=='tagged_in_photo')
			{
				$fbarray[$i]['fbfeedlink']  =$friends['data'][$i]['link'];
				$fbarray[$i]['feedid'] = $friends['data'][$i]['object_id'];
			}

			if(isset($friends['data'][$i]['message']))
				$fbarray[$i]['msg'] = $friends['data'][$i]['message'];
			elseif(isset($friends['data'][$i]['story']))
				$fbarray[$i]['msg'] = $friends['data'][$i]['story'];
			elseif(isset($friends['data'][$i]['description']))
				$fbarray[$i]['msg'] = $friends['data'][$i]['description'];

			$fbarray[$i]['fromid'] = $friends['data'][$i]['from']['id'];
			$fbarray[$i]['posttype'] = $friends['data'][$i]['type'];

			$fbarray[$i]['liked'] = 0;
			$a = $friends['data'][$i]['likes'];
			for($aa=0;$aa<count($a['data']);$aa++)
			{
				if($user==$a['data'][$aa]['id'])
					$fbarray[$i]['liked'] = 1;
			}
			$fbarray[$i]['likescount'] = $friends['data'][$i]['likes']['count'];
			if(isset($friends['data'][$i]['comments']['data']))
			{
				$g=0;
				foreach ($friends['data'][$i]['comments']['data'] as $comm)
				{
					$fbarray[$i]['comments'][$g] .='<br>'.$comm['from']['name'].': '.$comm['message'];
					$g++;
				}
			}

			if(isset($friends['data'][$i]['picture']))
				$fbarray[$i]['picurl'] = "<img src =".$friends['data'][$i]['picture']."><br>";


		}

	}
	catch (FacebookApiException $e) {
		error_log($e);
		$user = null;
		$writelog->fatal("Exception in Facebook. Please Check Configuration");
		$writelog->fatal($e->getMessage());
	}
}
else{
	$noConfiguration['facebook'] = true;
}

require_once 'TwitterConfigCheck.php';
global $twitConfig;
if($twitConfig){
	$code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/home_timeline', 'json'),array(
				'include_entities' => '1',
				'count' => 15));
	$retValue = json_decode($tmhOAuth->response['response']);
	for($j=0;$j<count($retValue);$j++)
	{

		$fbarray[$i]['source'] = 'tw';
		$fbarray[$i]['updated_time'] = strtotime($retValue[$j]->created_at);
		$fbarray[$i]['from_name'] = $retValue[$j]->user->name;
		$fbarray[$i]['statusid'] = $retValue[$j]->id;
		$fbarray[$i]['msg'] = $retValue[$j]->text;
		$fbarray[$i]['fromuser'] = $retValue[$j]->user->name;
		$fbarray[$i]['inreplyto'] = $retValue[$j]->in_reply_to_screen_name;
		$fbarray[$i]['fromuserscreenname'] = $retValue[$j]->user->screen_name;
		$fbarray[$i]['fromuserpic'] = $retValue[$j]->user->profile_image_url_https; 
		$fbarray[$i]['retweeted'] = $retValue[$j]->retweeted;
		$fbarray[$i]['retweet_count'] = $retValue[$j]->retweet_count;
		$fbarray[$i]['favorited'] = $retValue[$j]->favorited;
		$fbarray[$i]['followerscount'] = $retValue[$j]->user->followers_count;
		$fbarray[$i]['friendscount'] = $retValue[$j]->user->friends_count;
		$fbarray[$i]['tweetscount'] = $retValue[$j]->user->statuses_count;
		$fbarray[$i]['userdesc'] = $retValue[$j]->user->description;
		$fbarray[$i]['userurl'] = $retValue[$j]->user->url;
		$fbarray[$i]['userloc'] = $retValue[$j]->user->location;
		$fbarray[$i]['userlang'] = $retValue[$j]->user->lang;
		$fbarray[$i]['usergeo'] = $retValue[$j]->user->geo_enabled;
		$fbarray[$i]['usertimezone'] = $retValue[$j]->user->time_zone;
		$fbarray[$i]['userutcoffet'] = $retValue[$j]->user->utc_offset;
		$fbarray[$i]['usercreatedat'] =$retValue[$j]->user->created_at;
		$fbarray[$i]['userlistedcount'] =$retValue[$j]->user->listed_count;
		if(isset($retValue[$j]->entities->media))
		{
			$entity = $retValue[$j]->entities->media;
			if(isset($entity))
			{
				$mediaurl = $entity[0]->media_url;
				$fbarray[$i]['mediaimage'] ="<a href = $mediaurl target=_blank><img src = $mediaurl width=100 ></a>";
			}
		}


		$i++;
	}
}
else
$noConfiguration['twitter'] = true;
for($k=0;$k<count($fbarray);$k++)
{

	for($m=0;$m<count($fbarray);$m++)
		if($fbarray[$k]['updated_time']>$fbarray[$m]['updated_time'])
		{
			$tmparray = $fbarray[$m];
			$fbarray[$m] = $fbarray[$k];
			$fbarray[$k] =$tmparray;
		}
}
if(count($fbarray) == 0){
	echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
	echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

		<table border='0' cellpadding='5' cellspacing='0' width='98%'>
		<tbody><tr>
		<td rowspan='2' width='11%'><img src='". vtiger_imageurl('denied.gif', $theme) ."' ></td>
		<td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
		s='genHeaderSmall'> <strong>Please Configure Before Proceed</strong> </span></td>
		</tr>
		<tr>
		<td class='small' align='right' nowrap='nowrap'>
		<a href='index.php?module=Settings&action=SocioSettings'> Click Here for socialcrm configuration</a><br>
		</td>
		</tr>
		</tbody></table>
		</div>";
	echo "</td></tr></table>";
	exit;
}
else{
	//form the resposne string
	$responsestring = "<table>";
	for($n=0;$n<count($fbarray);$n++)
	{
		if($fbarray[$n]['source'] == 'tw')
		{
			$retweet_count = $fbarray[$n]['retweet_count'];
			if($retweet_count)
				$retweetedtext = $retweet_count;
			$retweeted = $fbarray[$n]['retweeted'];
			$statusid = $fbarray[$n]['statusid'];
			if($retweeted)
				$retweet = "<img src = modules/SocialFeeds/retweeted.png alt=Retwitted></img>";
			else
				$retweet = "<a onclick=retweet('$statusid')><img src = modules/SocialFeeds/retweet.png alt=Retweet /></a>";
			$favorited = $fbarray[$n]['favorited'];
			$favoritediv = "<a onclick = addFavourite('$statusid')><img src = modules/SocialFeeds/favorite.png alt=Favorite/></a>";
			if($favorited)
			{
				$favoritediv = "<a onclick = removeFavorite('$statusid')><img src = modules/SocialFeeds/favorited.png alt=Favorited /></a>";
			}
			$mediaimage ='';
			if(isset($fbarray[$n]['mediaimage']))
				$mediaimage = $fbarray[$n]['mediaimage'];
			$followerscount = $fbarray[$n]['followerscount'];
			$friendscount = $fbarray[$n]['friendscount'];
			$tweetscount =  $fbarray[$n]['tweetscount'];
			$userdesc =  $fbarray[$n]['userdesc'];
			$userurl = $fbarray[$n]['userurl'];
			$userloc = $fbarray[$n]['userloc'];
			$userlang = $fbarray[$n]['userlang'];
			$usergeo = $fbarray[$n]['usergeo'];
			$usertimezone = $fbarray[$n]['usertimezone'];
			$userutcoffet = $fbarray[$n]['userutcoffet'];
			$usercreatedat = $fbarray[$n]['usercreatedat'];
			$userlistedcount = $fbarray[$n]['userlistedcount'];
			$userutcoffet = $fbarray[$n]['userutcoffet'];
			$userutcoffet = $fbarray[$n]['userutcoffet'];
			$fromuserpic = $fbarray[$n]['fromuserpic'];
			$fromuser = $fbarray[$n]['fromuser'];
			$msg = '';
			$msg = $fbarray[$n]['msg'];
			if($userurl)
				$userurl = "<tr><td>Url: </td><td>$userurl</td></tr>";
			if($userloc)
				$userloc = "<tr><td>Location: </td><td>$userloc</td></tr>";
			if($userprotected)
				$userprotected ="<tr><td>Protected: </td><td>$userprotected</td></tr>";
			if($userlang)
				$userlang = "<tr><td>Language</td><td class=texttransform>$userlang</td></tr>";
			if($usergeo)
				$usergeo = "<tr><td>GEO enabled</td><td class=texttransform>Yes</td></tr>";
			if($usertimezone)
				$usertimezone = "<tr><td>Time Zone</td><td class=texttransform>$usertimezone</td></tr>";
			if($userutcoffet)
				$userutcoffet = "<tr><td>UTC offet</td><td class=texttransform>$userutcoffet</td></tr>";

			preg_match('/(http:\/\/[^\s]+)/', $msg, $text);
			$hypertext = "<a href=\"". $text[0] . "\" target=_blank>" . $text[0] . "</a>";
			$msgwithlink = preg_replace('/(http:\/\/[^\s]+)/', $hypertext, $msg);


			$profsummary = "<div id='foll$statusid' class='layerPopup' style='display:none;width:35%;margin-left:35%;height:auto;'>";
			$profsummary .= "<table cellspacing=0 cellpadding=5 width=100% border=0 class='layerHeadingULine'><tr class=singletweet1><td class='genHeaderSmall'>ProfileSummary</td><td align='right'><a href=javascript:fninvsh('foll$statusid');><img border='0' align='absmiddle' src='themes/images/close.gif'></a></td></tr>";
			$profsummary .= "<tr><td>Followers:</td><td>$followerscount</td></tr><tr><td>Following: </td><td>$friendscount</td></tr><tr><td>Tweets:</td><td>$tweetscount</td></tr><tr><td>Bio</td><td>$userdesc</td></tr>$userlistedcount $userurl $userloc $userprotected $userlang $usergeo $usertimezone $userutcoffset $usercreatedat</table>";
			$profsummary .= "</div>";

			$responsestring .="<tr class = rowhover><td><table width=100%><tr><td class=singletweet1><div><div class=floatleft><img src = $fromuserpic class = profpic></div><div class = profname><a onclick=showuserdesc('$statusid')> $fromuser</a> <div id = 'loading_$statusid' style = 'display:none;float:right;'>  <img src = 'themes/images/loading.gif' alt = 'Loading' title = 'Loading'> </div> </div>$profsummary</div><br><br><br></td></tr>";
			$responsestring .= "<tr><td class=singletweet><br><div class = tweetdiv> $msgwithlink</div><br>";
			$responsestring .= "<div id =retweet$statusid class = floatright>$retweet</div>";
			$responsestring .= "<div id =reply$statusid class = floatright><a href='javascript:;' onclick=showReplyTweet('$statusid')><img src=modules/SocialFeeds/reply.png alt=Reply></a></div>";
			$responsestring .= "<div id=fav$statusid class = floatright>$favoritediv</div>";
			$responsestring .="<div class = commentdiv name = commentdiv id=$statusid><input type =textbox id=txt$statusid class=txtbox><input class='crmbutton small create' type = button onclick=replyTweet('$statusid') value = Reply> <div id = 'comment_$statusid' style = 'color:green; font-size:14px; padding-top:7px; display:none; '>  Posted Successfully </div> </div>";
			$responsestring .= "<br>".$mediaimage;
			$responsestring .= "</td></tr></table></td></tr>";
		}//exits twitter
		else if($fbarray[$n]['source'] == 'fb')
		{
			$fromid = $fbarray[$n]['fromid'];
			$feedid = $fbarray[$n]['feedid'];
			$fromname1 = $fbarray[$n]['from_name'];
			$profsummary = "<div id='prof$feedid' class='layerPopup' style='display:none;width:35%;margin-left:35%;height:auto;'><table cellspacing=0 cellpadding=5 width=100% border=0 class='layerHeadingULine'><tr class=singletweet1><td class='genHeaderSmall'>ProfileSummary</td><td align='right'><a href=javascript:fninvsh('prof$feedid');><img border='0' align='absmiddle' src='themes/images/close.gif'></a></td></tr><tr><td></td></tr><tr><td id='profinfo$feedid'></td></tr></table></div><div id = 'loading_$feedid' style = 'display:none;'>  <img src = 'themes/images/loading.gif' alt = 'Loading' title = 'Loading'> </div>";

			$msg = $fbarray[$n]['msg'];
			$comments = '';
			for($o=0;$o<count($fbarray[$n]['comments']);$o++)
			{
				$comments .= $fbarray[$n]['comments'][$o];
			}

			$likescount = $fbarray[$n]['likescount'];
			$liked = $fbarray[$i]['liked'];
			$fromname =str_replace(' ','',$fromname1);
			$posttype = $fbarray[$i]['posttype'];
			$forlikes = '';
			if($liked == 0)
				$forlikes = "<br><div id=like$feedid class = 'floatright'><a onclick=likeFeed('$feedid','$fromname','$posttype');>Like </a></div>";
			else
				$forlikes = "<br><div id=like$feedid class = 'floatright'><a onclick=dislikeFeed('$feedid','$fromname','$posttype');>Liked </a></div>";
			$forcomments = "<div class='floatright commentpadding'> <a onclick=commentDiv('$feedid');>Comment</a> </div>";
			$fromuserpic = "https://graph.facebook.com/$fromid/picture";

			$responsestring .= "<tr class='rowhover'><td><table width=100%><tr><td class='singletweet1'><div class='prof_img floatleft'><img src ='{$fromuserpic}' class= 'profpic'></div><div class = 'profname floatleft'><a onclick=getfbprofsummary('$fromid','$feedid')>".$fromname1."</a><br><br></div>$profsummary</td></tr>";

			$responsestring .= "<tr><td class=singletweet><br><div class = tweetdiv> $msg</div><div id =$feedid class=commentdiv>$comments<br><input type =textbox id=txt$feedid class=txtbox><input class='crmbutton small create' type = button onclick=commentOnFeed('$feedid') value = Post Comment></div>$forlikes $forcomments ";
			$picurl = $fbarray[$n]['picurl'];
			$responsestring .= "<br>$picurl</td></tr></table></td></tr>";
		}
	}
}
$resonsestring .= "</table>";
if(count($noConfiguration) > 0){
	if($noConfiguration['facebook'])
		$responsestring = "<div style='color:red;'>Facebook not yet configured. <a href='index.php?module=Settings&action=SocioSettings'> Configure now.</a></div><br/>" . $responsestring;
	if($noConfiguration['twitter'])
		$responsestring = '<div style="color:red;">Twitter not yet configured. <a href="index.php?module=Settings&action=SocioSettings">Configure now.</a></div><br/>' . $responsestring;
}
	echo $responsestring;
