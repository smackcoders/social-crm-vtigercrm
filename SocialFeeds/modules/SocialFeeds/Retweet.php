<?php
require_once('include/logging.php');
$writelog =& LoggerManager::getLogger('SOCIALCRM');
$retweetstatusid = $_REQUEST['statusid'];
require_once("TwitterConfig.php");
$code = $tmhOAuth->request('POST', $tmhOAuth->url("1.1/statuses/retweet/$retweetstatusid"));
if ($code == 200) 
{
	echo 'Retweeted Successfully';
}
else
{
	$writelog->fatal("Please Check Twitter Configuration");
	echo $code.' error, cant be retweeted';
}
