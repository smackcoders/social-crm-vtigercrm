<?php
require 'tmhOAuth.php';
require 'tmhUtilities.php';

# Getting the Twitter id from SocioSettings Table
$getTwitterIDQuery = $adb->pquery("select twitter_usecret, twitter_utoken, twitter_csecret, twitter_ckey from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getTwitterIDQuery);

if($queryCount != 0)
{
        $usecret = $adb->query_result($getTwitterIDQuery,0,"twitter_usecret");
        $utoken = $adb->query_result($getTwitterIDQuery,0,"twitter_utoken");
        $csecret = $adb->query_result($getTwitterIDQuery,0,"twitter_csecret");
        $ckey = $adb->query_result($getTwitterIDQuery,0,"twitter_ckey");
}

if($queryCount == 0)
{
        echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'><img src='". vtiger_imageurl('denied.gif', $theme) ."' ></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> Twitter Id Not provided </strong> </span></td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                <a href='index.php?module=Settings&action=SocioSettings'> Click Here to Enter the Twitter ID</a><br>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
        exit;

}

$tmhOAuth = new tmhOAuth(array(
  'consumer_key'    => $ckey,
  'consumer_secret' => $csecret,
  'user_token'      => $utoken,
  'user_secret'     => $usecret,
));

if($_REQUEST['limit'] > 200)
{
	$_REQUEST['limit'] = 200;
	$limitexceeds = 1;
}

$code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/home_timeline', 'json'),array(
  'include_entities' => '1',
'count' => $_REQUEST['limit']));

print $code;
