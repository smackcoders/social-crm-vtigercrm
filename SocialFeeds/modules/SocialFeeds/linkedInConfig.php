<?php
global $adb;
# Getting the Linkedin id from SocioSettings Table
$getLinkedIDQuery = $adb->pquery("select linkedintoken1, linkedintoken2, linkedinauth1, linkedinauth2 from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getLinkedIDQuery);

if($queryCount != 0)
{
        $token1 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken1");
        $token2 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken2");
        $auth1 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth1");
        $auth2 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth2");
}

if( $queryCount == 0 || ( empty($token2) && empty($token1) && empty($auth1) && empty($auth2) ))
{
        echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'><img src='". vtiger_imageurl('denied.gif', $theme) ."' ></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> LinkedIn Id Not provided </strong> </span></td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                <a href='index.php?module=Settings&action=SocioSettings'> Click Here to Enter the Linkedin Id's </a><br>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
        exit;
}

$oauth = new OAuth($token1, $token2);
$oauth->setToken($auth1, $auth2);
