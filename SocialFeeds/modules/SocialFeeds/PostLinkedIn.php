<?php
require_once('linkedin_3.2.0.class.php');
require_once('include/logging.php');
$writelog =& LoggerManager::getLogger('SOCIALCRM');
global $current_user, $currentModule,$adb;
/* get linked in app credentials from db */
$getLinkedIDQuery = $adb->pquery("select linkedintoken1, linkedintoken2, linkedinauth1, linkedinsecret, linkedintoken, linkedinauth2 from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getLinkedIDQuery);
if($queryCount != 0){
        $token1 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken1");
        $token2 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken2");
        $auth1 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth1");
        $auth2 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth2");
        $accToken =$adb->query_result($getLinkedIDQuery,0,"linkedintoken");
        $secToken = $adb->query_result($getLinkedIDQuery,0,"linkedinsecret");
}
  $API_CONFIG = array(
          'appKey'=> $token1,  
          'appSecret'    => $token2,
          'callbackUrl'  => NULL
  );
        $OBJ_linkedin = new LinkedIn($API_CONFIG);
        $setToken = array('oauth_token' =>$accToken, 'oauth_token_secret' => $secToken);
        $OBJ_linkedin->setTokenAccess($setToken); 
        $response = $OBJ_linkedin->updateNetwork($_REQUEST['texttopost']);
        if($response['success'] === TRUE) {
        // status has been updated
	echo 'success';
        } else {
        // an error occured
        echo "failure";
	$writelog->fatal("Exception - Check LinkedIN Configuration");
      }

/*$texttopost = $_REQUEST['texttopost'];
require_once('linkedInConfig.php');
$body_xml = "<?xml version='1.0' encoding='UTF-8'?>
<share>
   <comment></comment>
   <content>
      <title>".$texttopost."</title>                                                     
      <submitted-url>http://smackcoders.com</submitted-url>      
      <submitted-image-url></submitted-image-url>  
   </content>
   <visibility>
      <code>anyone</code>
   </visibility>
</share>";
$api_url = "http://api.linkedin.com/v1/people/~/shares";
$oauth->fetch($api_url, $body_xml, OAUTH_HTTP_METHOD_POST, array("Content-Type" => "text/xml"));
echo 'Posted Sucessfully';
*/
