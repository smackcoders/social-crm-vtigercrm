<?php
require_once('linkedin_3.2.0.class.php');
require_once('include/logging.php');
error_reporting(0);
$writelog =& LoggerManager::getLogger('SOCIALCRM');
global $current_user, $currentModule,$adb;
/* get linked in app credentials from db */
$getLinkedIDQuery = $adb->pquery("select linkedintoken1, linkedintoken2, linkedinauth1, linkedinsecret, linkedintoken, linkedinauth2 from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getLinkedIDQuery);
if($queryCount != 0){
	$token1 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken1");
	$token2 = $adb->query_result($getLinkedIDQuery,0,"linkedintoken2");
	$auth1 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth1");
	$auth2 = $adb->query_result($getLinkedIDQuery,0,"linkedinauth2");
	$accToken =$adb->query_result($getLinkedIDQuery,0,"linkedintoken");
	$secToken = $adb->query_result($getLinkedIDQuery,0,"linkedinsecret");
}
  $API_CONFIG = array(
          'appKey'=> $token1,
          'appSecret'    => $token2,
          'callbackUrl'  => NULL
  );
	$OBJ_linkedin = new LinkedIn($API_CONFIG);
	$setToken = array('oauth_token' =>$accToken, 'oauth_token_secret' => $secToken);
        $OBJ_linkedin->setTokenAccess($setToken);

	$query    = '';
        $response = $OBJ_linkedin->updates($query);
        if($response['success'] === TRUE) {
        	$updates = new SimpleXMLElement($response['linkedin']);
                if((int)$updates['total'] > 0) {
			$val = $updates->update;
			$feed = "<table>";

			for($i=0;$i<count($val);$i++){
				$skills = '';
				$profurl = (string) $val[$i]->{'update-content'}->person->{'api-standard-profile-request'}->url;
				$profsummary = "<div id='prof$i' class='layerPopup' style='display:none;width:35%;margin-left:35%;height:auto;'><table cellspacing=0 cellpadding=5 width=100% border=0 class='layerHeadingULine'><tr class=singletweet1><td class='genHeaderSmall'>ProfileSummary</td><td align='right'><a href=javascript:fninvsh('prof$i');><img border='0' align='absmiddle' src='themes/images/close.gif'></a></td></tr><tr><td></td></tr><tr><td id='profinfo$i'></td></tr></table></div>";

				$type = (string) $val[$i]->{'update-type'};
				$name = (string)$val[$i]->{'update-content'}->person->{'first-name'}.' '.(string)$val[$i]->{'update-content'}->person->{'last-name'};
				$id =$val[$i]->{'update-content'}->person->id;

				if($type == "STAT"){
					$connid =(string) $val[$i]->{'update-content'}->person->id;
					$nooflikes = (int)$val[$i]->{'num-likes'};
					$pic = (string)$val[$i]->{'update-content'}->person->{'picture-url'};
					$status = (string)$val[$i]->{'update-content'}->person->{'current-status'};

					$feed .= "<tr class = rowhover><td><table width=100%><tr><td class=singletweet1><div><div class=floatleft><img src = $pic class = profpic></div><div class = profname><a href='#prof$i' onclick=showuserdescLinkedin('$i','{$profurl}')> $name</a></div>$profsummary</div><br><br><br></td></tr>";
					$feed .= "<tr><td class=singletweet><br><div class = tweetdiv> $status</div><br>";
				}
				else if($type == 'PROF'){
					$skills ='';
					$connid = (string) $val[$i]->{'update-content'}->person->id;
					$nooflikes = (int) $val[$i]->{'num-likes'};
					$pic = (string) $val[$i]->{'update-content'}->person->{'picture-url'};
					$url = $val[$i]->{'update-content'}->person->{'site-standard-profile-request'}->url;
					$skills =$val[$i]->{'update-content'}->person->skills;
					if(($skills!= ' ')&&(isset($skills))){
					$skillscount = (int)$skills->attributes()->count;
					$skill = '';
						for($s=0;$s<$skillscount;$s++){
							if($s==0)
								$skill .= (string)$skills->skill[$s]->skill->name;
							else if($s==($skillscount-1))
								$skill .= " and ".(string)$skills->skill[$s]->skill->name;
							else
								$skill .= ", ".(string)$skills->skill[$s]->skill->name;
						}
						$status = $name." updated their skills in $skill";
					}
					else
						$status = "$name updated their <a href = '{$url}' target=_blank >Profile</a>";
						$feed .= "<tr class = rowhover><td><table width=100%><tr><td class=singletweet1><div><div class=floatleft><img src = $pic class = profpic></div><div class = profname><a href='#prof$i' onclick=showuserdescLinkedin('$i','{$profurl}')>  $name</a></div>$profsummary</div><br><br><br></td></tr>";
						$feed .= "<tr><td class=singletweet><br><div class = tweetdiv> $status</div><br>";
				}
				else if($type == 'NCON'){
					$pic = ' ';
					$nooflikes = '';
					$connid = $val[$i]->{'update-content'}->person->id;
					if(isset($val[$i]->{'num-likes'}))
						$nooflikes = $val[$i]->{'num-likes'};
					if(isset($val[$i]->{'update-content'}->person->{'picture-url'}))
						$pic =$val[$i]->{'update-content'}->person->{'picture-url'};
					$url = $val[$i]->{'update-content'}->person->{'site-standard-profile-request'}->url;
					if(($pic)&&($pic!=' ')){
						//echo $i."<img src=$pic>".'<br>';
					}
					else{
						$pic = "add no image location here";
					}
					$status = "$name is now a Connection";
$feed .= "<tr class = rowhover><td><table width=100%><tr><td class=singletweet1><div><div class=floatleft><img src = $pic class = profpic></div><div class = profname><a href='#prof$i' onclick=showuserdescLinkedin('$i','{$profurl}')>  $name</a></div>$profsummary</div><br><br><br></td></tr>";
$feed .= "<tr><td class=singletweet><br><div class = tweetdiv> $status</div><br>";
				}//exits type if
				else if($type == 'CONN'){
					//friends section begins
					$pic = ' ';
					if(isset($val[$i]->{'update-content'}->person->{'picture-url'}))
						$pic =$val[$i]->{'update-content'}->person->{'picture-url'};
					if(($pic)&&($pic!=' ')){
						//echo $i."<img src=$pic>".'<br>';
					}
					else{
						$pic = '';//"add no image location here";
					}
					$frdpic = ' ';
					if(isset($val[$i]->{'update-content'}->person->connections->values[0]->{'picture-url'}))
						$frdpic =$val[$i]->{'update-content'}->person->connections->values[0]->{'picture-url'};
					if(($frdpic)&&($frdpic!=' ')){
						$addfrdpic = "<br><img src=$frdpic>";
					}
					else{
						$addfrdpic = "";
					}
					$friendname = $name;
					$friendsfriendsname = (string)$val[$i]->{'update-content'}->person->connections->person->{'first-name'}.' '.(string)$val[$i]->{'update-content'}->person->connections->person->{'last-name'};

					$status = "$friendname is now in connected to $friendsfriendsname"; 

					$feed .= "<tr class = rowhover><td><table width=100%><tr><td class=singletweet1><div><div class=floatleft><img src = $pic class = profpic></div><div class = profname><a href='#prof$i' onclick=showuserdescLinkedin('$i','{$profurl}')>  $friendname</a></div>$profsummary</div><br><br><br></td></tr>";
					$feed .= "<tr><td class=singletweet><br><div class = tweetdiv> $status</div><br><div class='floatleft'>$addfrdpic</div>";
				}
				else if($type == "PICU"){
					$status = "$name has updated his profile picture";
					$pic = ' ';
					if(isset($val[$i]->{'update-content'}->person->{'picture-url'}))
						$pic =$val[$i]->{'update-content'}->person->{'picture-url'};
					if(($pic)&&($pic!=' ')){
						//echo $i."<img src=$pic>".'<br>';
					}
					else 
						$pic = '';//"add no image location here";
					$feed .= "<tr class = rowhover><td><table width=100%><tr><td class=singletweet1><div><div class=floatleft><img src = $pic class = profpic></div><div class = profname><a href='#prof$i' onclick=showuserdescLinkedin('$i','{$profurl}')>  $name</a></div>$profsummary</div><br><br><br></td></tr>";
					$feed .= "<tr><td class=singletweet><br><div class = tweetdiv> $status</div><br>";
				}
if($val[$i]->{'update-comments'}){
//var_dump($val[$i]->{'update-comments'}->attributes());die;
}
				/* add options to comment and like */
				if(($type == "STAT")||($type == "PICU")||($type == 'CONN')||($type == 'NCON')||($type == 'PROF')){
					if((bool)$val[$i]->{'is-commentable'}){
						$comment = '';
						if(($val[$i]->{'update-comments'})&&(((int)$val[$i]->{'update-comments'}->attributes()->total) > 0)&&($val[$i]->{'update-comments'})){
							$comments = $val[$i]->{'update-comments'}->{'update-comment'};
							for($r=0;$r<count($comments);$r++){
								$comment .= "<img src='".$comments[$r]->person->{'picture-url'}."' class='profpic'>".$comments[$r]->person->{'first-name'}.': '.$comments[$r]->comment.'<br/>';
							}
						}
						$updatekey = $val[$i]->{'update-key'};
						$feed .= "<div class = commentdiv name = commentdiv id={$i}{$id}>$comment<input type =textbox id=txt$i$id class=txtbox><input class='crmbutton small create' type = button onclick=sendmessagelinked('$i','$id','$updatekey') value = Comment></div>";
					}//exits if commentable
				if((bool)$val[$i]->{'is-likable'}){
					$feed .= "<div class='floatright' id=like{$i}{$updatekey} >";
					if($val[$i]->{'is-liked'})
						$feed .="<a onclick=dislikelinkedin('{$updatekey}','{$i}')>Liked</a>";
					else
						$feed .="<a onclick=likelinkedin('{$updatekey}','{$i}')>Like</a>";
					$feed .="</div>";
				}
				if((bool)$val[$i]->{'is-commentable'})
					$feed .= "<div class='floatright commentpadding'><a href='#$i$id' onclick=showSendMsgLink('$i$id')>Comment</div>";
					$feed .= '</td></tr></table></td></tr>';
			}

		}
$feed .= "</table>";
echo $feed;

		}
		else
		echo '<table width="98%" cellspacing="0" cellpadding="5" border="0">
                <tbody><tr>
                </td>
                <td width="70%" nowrap="nowrap" style="border-bottom: 1px solid rgb(204, 204, 204);"><span class="genHeaderSmall">No Updates to Show</span></td>
                </tr>
                <tr>
                </tr>
                </tbody></table>';
	}
	else{
	echo "<table border='0' cellpadding='5' cellspacing='0' width='100%' height='450px'><tr><td align='center'>";
        echo "<div style='border: 3px solid rgb(153, 153, 153); background-color: rgb(255, 255, 255); width: 55%; position: relative; z-index: 10000000;'>

                <table border='0' cellpadding='5' cellspacing='0' width='98%'>
                <tbody><tr>
                <td rowspan='2' width='11%'><img src='". vtiger_imageurl('denied.gif', $theme) ."' ></td>
                <td style='border-bottom: 1px solid rgb(204, 204, 204);' nowrap='nowrap' width='70%'><span clas
                s='genHeaderSmall'> <strong> Check LinkedIn Credentials </strong> </span></td>
                </tr>
                <tr>
                <td class='small' align='right' nowrap='nowrap'>
                <a href='index.php?module=Settings&action=SocioSettings'> Click Here to go Settings.</a><br>
                </td>
                </tr>
                </tbody></table>
                </div>";
        echo "</td></tr></table>";
		$writelog->fatal("Error in LinkedIn Configuration");
        }

