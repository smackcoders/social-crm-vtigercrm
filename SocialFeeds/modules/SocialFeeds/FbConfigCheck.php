<?php
require 'src/facebook.php';
global $adb;
# Getting the Facebook Token, Secret, App Id from SocioSettings Table

$fbConfig = true;
$getFacebookIDQuery = $adb->pquery("select facebook_app, facebook_secret, facebook_access from vtiger_sociosettings where id = 1",array());
$queryCount = $adb->num_rows($getFacebookIDQuery);
$fb = true;
if($queryCount != 0)
{
	$facebookapp = $adb->query_result($getFacebookIDQuery,0,"facebook_app");
	$facebooksecret = $adb->query_result($getFacebookIDQuery,0,"facebook_secret");
	$facebookaccess = $adb->query_result($getFacebookIDQuery,0,"facebook_access");
	if(empty($facebookapp) || empty($facebooksecret) || empty($facebookaccess))
	{
		$fb = false;
	}
}

if($queryCount == 0 || empty($fb) || !isset($fb))
{
	$fbConfig = false;
}
$facebook = new Facebook(array(
			'appId'  => $facebookapp,
			'secret' => $facebooksecret,
			));

$facebook->setAccessToken($facebookaccess);
